import React from 'react';
import {ActivityIndicator, TouchableOpacity, View} from 'react-native';
import PlusSvg from '../../assets/svg/components/PlusSvg';
import colors from '../../constants/color';
import TextTypes from '../Text';
import TickSvg from '../../assets/svg/tick.svg';

const NormalButton = ({
  mode = 'normal',
  text,
  customContent,
  withShadow = true,
  showLoading = false,
  onPress,
  containerStyle,
}) => {
  let backgroundColor = colors.seagreen;
  let textColor = colors.white;

  if (mode == 'ghost') {
    backgroundColor = colors.grey;
    textColor = colors.seagreen;
  }

  if (mode == 'inactive') {
    backgroundColor = colors.grey;
    textColor = colors.darkGrey;
  }

  return (
    <TouchableOpacity
      disabled={showLoading}
      onPress={onPress}
      style={[
        {
          backgroundColor,
          borderRadius: 10,
          paddingVertical: 12,
          alignItems: 'center',
          justifyContent: 'center',
          shadowOpacity: withShadow ? 0.25 : 0,
          elevation: withShadow ? 20 : 0,
          shadowOffset: {
            width: 0,
            height: 5,
          },
        },
        containerStyle,
      ]}>
      {customContent ? (
        customContent
      ) : showLoading ? (
        <ActivityIndicator
          size="small"
          color={mode == 'ghost' ? colors.seagreen : colors.white}
        />
      ) : (
        <TextTypes.RegularText style={{color: textColor}}>
          {text}
        </TextTypes.RegularText>
      )}
    </TouchableOpacity>
  );
};

const NormalButtonCircle = ({mode = 'normal', onPress}) => {
  let backgroundColor = colors.seagreen;
  let borderWidth = 0;
  let svgColor = colors.white;
  let disabled = false;

  if (mode == 'ghost') {
    backgroundColor = colors.white;
    borderWidth = 1;
    svgColor = colors.seagreen;
  }

  if (mode == 'inactive') {
    backgroundColor = colors.grey;
    svgColor = colors.darkGrey;
    disabled = true;
  }

  return (
    <TouchableOpacity
      onPress={onPress}
      disabled={disabled}
      style={{
        width: 36,
        height: 36,
        borderRadius: 18,
        backgroundColor,
        borderWidth,
        borderColor: colors.seagreen,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <PlusSvg color={svgColor} />
    </TouchableOpacity>
  );
};

const SaveButton = ({onPress, containerStyles}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[
        {
          width: 96,
          alignItems: 'center',
          justifyContent: 'center',
          paddingVertical: 12,
          backgroundColor: colors.seagreen,
          borderRadius: 10,
          shadowOpacity: 0.25,
          elevation: 20,
          shadowOffset: {
            width: 0,
            height: 5,
          },
        },
        containerStyles,
      ]}>
      <TextTypes.RegularText>Save</TextTypes.RegularText>
    </TouchableOpacity>
  );
};

const TopicButton = ({text, onPress}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        width: 119,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
        backgroundColor: colors.darkGreen,
        paddingVertical: 6,
      }}>
      <TextTypes.RegularText>{text}</TextTypes.RegularText>
    </TouchableOpacity>
  );
};

const TickToggle = ({toggle}) => {
  return (
    <View
      style={{
        width: 24,
        height: 24,
        borderRadius: 12,
        borderColor: colors.black,
        borderWidth: toggle ? 0 : 1,
        backgroundColor: toggle ? colors.seagreen : colors.white,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      {toggle && <TickSvg />}
    </View>
  );
};

const Button = {
  NormalButton,
  NormalButtonCircle,
  SaveButton,
  TopicButton,
  TickToggle,
};

export default Button;
