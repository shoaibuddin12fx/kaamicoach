import React from 'react';
import {View} from 'react-native';
import TextTypes from '../Text';

const GlowBlob = ({
  text,
  customContent,
  r = 55,
  g = 192,
  b = 176,
  containerStyles,
  textStyles,
}) => {
  return (
    <View
      style={[
        {
          width: 55,
          height: 55,
          borderRadius: 27.5,
          backgroundColor: `rgba(${r}, ${g}, ${b}, 0.5)`,
          alignItems: 'center',
          justifyContent: 'center',
        },
        containerStyles,
      ]}>
      <View
        style={{
          width: 32,
          height: 32,
          borderRadius: 16,
          backgroundColor: `rgb(${r}, ${g}, ${b})`,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        {customContent ? (
          customContent
        ) : (
          <TextTypes.RegularText
            style={[
              {
                fontSize: 22,
                fontFamily: 'Nunito-Regular',
                color: '#F4F4F4',
                fontWeight: '600',
              },
              textStyles,
            ]}>
            {text}
          </TextTypes.RegularText>
        )}
      </View>
    </View>
  );
};

export default GlowBlob;
