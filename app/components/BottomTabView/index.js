import React, {useCallback} from 'react';
import {SafeAreaView, ScrollView} from 'react-native';
import colors from '../../constants/color';
import {Col} from '../core/Layout';
import HeaderBackButton from '../HeaderBackButton';

const BottomTabView = ({
  navigation,
  useScroll = true,
  children,
  containerStyles,
  headerText,
  svgContainerStyles,
  customNavigate,
}) => {
  const goBack = useCallback(() => {
    if (navigation.canGoBack()) navigation.goBack();
  }, [navigation]);

  return (
    <SafeAreaView style={{flexGrow: 1, backgroundColor: colors.white}}>
      {useScroll ? (
        <ScrollView
          contentContainerStyle={{paddingHorizontal: 25}}
          showsVerticalScrollIndicator={false}
          bounces={false}
          keyboardShouldPersistTaps="handled">
          <HeaderBackButton
            text={headerText}
            onPress={customNavigate ? customNavigate : goBack}
            svgContainerStyles={svgContainerStyles}
          />
          {children}
        </ScrollView>
      ) : (
        <Col style={[{paddingHorizontal: 15}, containerStyles]}>
          <HeaderBackButton
            text={headerText}
            onPress={goBack}
            svgContainerStyles={[{paddingLeft: 10}, svgContainerStyles]}
          />
          {children}
        </Col>
      )}
    </SafeAreaView>
  );
};

export default BottomTabView;
