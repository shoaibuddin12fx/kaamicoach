import React from 'react';
import {Text} from 'react-native';
import colors from '../../constants/color';

const LightText = ({children, style, ...props}) => {
  return (
    <Text
      style={[
        {fontFamily: 'Nunito-Light', fontSize: 14, color: colors.white},
        style,
      ]}
      {...props}>
      {children}
    </Text>
  );
};

const RegularText = ({children, style, ...props}) => {
  return (
    <Text
      style={[
        {fontFamily: 'Nunito-Regular', fontSize: 18, color: colors.white},
        style,
      ]}
      {...props}>
      {children}
    </Text>
  );
};

const SemiBoldText = ({children, style, ...props}) => {
  return (
    <Text
      style={[
        {fontFamily: 'Nunito-SemiBold', fontSize: 24, color: colors.white},
        style,
      ]}
      {...props}>
      {children}
    </Text>
  );
};

const TextTypes = {
  LightText,
  RegularText,
  SemiBoldText,
};

export default TextTypes;
