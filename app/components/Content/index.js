import React from 'react';
import {
  Dimensions,
  FlatList,
  Image,
  KeyboardAvoidingView,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import SendSvg from '../../assets/svg/components/chat/SendSvg';
import RightSvg from '../../assets/svg/components/RightSvg';
import Button from '../Button';
import colors from '../../constants/color';
import {Col, Row} from '../core/Layout';
import TextTypes from '../Text';
import AddSvg from '../../assets/svg/components/additional/AddSvg';
import CrossSvg from '../../assets/svg/components/additional/CrossSvg';
import BookmarkSvg from '../../assets/svg/components/header/BookmarkSvg';

const {width} = Dimensions.get('window');

const ArticleCard = ({
  author,
  status,
  title,
  description,
  image,
  containerStyles,
  showBookmark = false,
  onPress,
}) => {
  return (
    <View style={[{backgroundColor: colors.grey}, containerStyles]}>
      <View
        style={{
          height: 175,
          marginBottom: 4,
        }}>
        <Image source={{uri: image}} style={{height: 175}} resizeMode="cover" />
        <View
          style={{
            backgroundColor: '#C4C4C4',
            position: 'absolute',
            top: 0,
            right: 0,
          }}>
          <TextTypes.RegularText
            style={{fontSize: 12, paddingVertical: 2, paddingHorizontal: 8}}>
            Parenting
          </TextTypes.RegularText>
        </View>

        {showBookmark && (
          <View
            style={{
              backgroundColor: '#C4C4C4',
              position: 'absolute',
              top: 0,
              left: 0,
            }}>
            <BookmarkSvg fill="#37C0B0" />
          </View>
        )}
      </View>

      <View>
        <View
          style={{
            flexDirection: 'row',
            marginBottom: 13,
            paddingHorizontal: 12,
          }}>
          <View
            style={{
              width: 35,
              height: 35,
              borderRadius: 17.5,
              backgroundColor: '#C4C4C4',
            }}></View>

          <View style={{marginLeft: 6}}>
            <TextTypes.RegularText
              style={{
                color: colors.black,
                fontSize: 12,
                fontFamily: 'Nunito-Regular',
              }}>
              {author}
            </TextTypes.RegularText>

            <TextTypes.RegularText
              style={{
                color: colors.seagreen,
                fontSize: 12,
                fontFamily: 'Nunito-Regular',
              }}>
              {status}
            </TextTypes.RegularText>
          </View>
        </View>

        <TextTypes.RegularText
          style={{
            color: colors.pureBlack,
            marginBottom: 3,
            paddingHorizontal: 12,
          }}>
          {title}
        </TextTypes.RegularText>

        {description && (
          <TextTypes.RegularText
            style={{
              color: '#535353',
              fontSize: 14,
              marginBottom: 12.5,
              paddingHorizontal: 12,
            }}>
            {description}
          </TextTypes.RegularText>
        )}

        <View
          style={{
            borderBottomWidth: 1,
            borderBottomColor: '#C2C2C2',
            marginBottom: 7.5,
          }}
        />

        <TouchableOpacity
          onPress={onPress}
          style={{
            flexDirection: 'row',
            alignItems: 'flex-start',
            paddingHorizontal: 12,
          }}>
          <TextTypes.RegularText
            style={{
              color: colors.pureBlack,
              fontSize: 14,
              marginBottom: 8,
            }}>
            Read More
          </TextTypes.RegularText>

          <RightSvg style={{marginTop: 4, marginLeft: 5}} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const DailyActivityCard = () => {
  return (
    <View
      style={{
        borderWidth: 1,
        borderColor: colors.seagreen,
        borderRadius: 10,
        paddingHorizontal: 15,
        paddingVertical: 20,
        shadowOpacity: 0.25,
        elevation: 20,
        shadowOffset: {
          width: 0,
          height: 5,
        },
        backgroundColor: colors.white,
      }}>
      <TextTypes.RegularText
        style={{
          color: colors.pureBlack,
          fontSize: 14,
          fontFamily: 'Nunito-Regular',
          marginBottom: 10,
        }}>
        Activity Three
      </TextTypes.RegularText>

      <TextTypes.RegularText
        style={{color: colors.pureBlack, marginBottom: 10}}>
        Why is Bonding Important?
      </TextTypes.RegularText>

      <TextTypes.RegularText
        style={{color: colors.pureBlack, marginBottom: 14, fontWeight: '400'}}>
        The most important stage for brain development is the beginning of life,
        starting in the womb and then the first year of life...
      </TextTypes.RegularText>

      <Button.NormalButton
        withShadow={false}
        customContent={
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <TextTypes.RegularText>Complete Activity</TextTypes.RegularText>
            <RightSvg color={colors.white} style={{marginLeft: 10}} />
          </View>
        }
      />
    </View>
  );
};

const Message = ({isBot, content, userInitial}) => {
  return (
    <View>
      <View
        style={{
          width: 24,
          height: 24,
          borderRadius: 12,
          alignSelf: isBot ? 'flex-end' : 'flex-start',
          backgroundColor: isBot ? colors.grey : colors.seagreen,
          marginBottom: 14,
        }}>
        <TextTypes.RegularText
          style={{
            color: isBot ? colors.seagreen : colors.white,
            textAlign: 'center',
            textAlignVertical: 'center',
          }}>
          {isBot ? 'K' : userInitial}
        </TextTypes.RegularText>
      </View>
      <View
        style={{
          borderRadius: 10,
          borderTopRightRadius: isBot ? 0 : 10,
          borderTopLeftRadius: isBot ? 10 : 0,
          backgroundColor: isBot ? colors.seagreen : colors.grey,
          alignSelf: isBot ? 'flex-end' : 'flex-start',
          maxWidth: width * 0.7,
          paddingVertical: 10,
          paddingHorizontal: 20,
          marginBottom: 15,
        }}>
        <TextTypes.RegularText
          style={{
            color: isBot ? colors.grey : colors.pureBlack,
            fontFamily: 'Nunito-Regular',
            fontSize: 14,
            fontWeight: '400',
          }}>
          {content}
        </TextTypes.RegularText>
      </View>
    </View>
  );
};

const MessageList = ({data, onSend, responses}) => {
  return (
    <>
      <KeyboardAvoidingView style={{flexGrow: 1}} behavior="padding">
        <FlatList
          data={data}
          keyExtractor={(item, index) => index.toString()}
          contentContainerStyle={{paddingHorizontal: 25, flexGrow: 1}}
          keyboardShouldPersistTaps="handled"
          renderItem={({item}) => {
            return (
              <Message
                isBot={item.isBot}
                userInitial={item.userInitial}
                content={item.content}
              />
            );
          }}
        />

        <View
          style={{
            flexDirection: 'row',
            paddingLeft: 15,
            paddingRight: 11,
            paddingTop: 14,
            paddingBottom: 8,
            borderTopWidth: 1,
            borderTopColor: colors.grey,
          }}>
          <TextInput
            placeholder="Write your message here..."
            placeholderTextColor={colors.lightGrey4}
            style={{
              borderWidth: 1,
              flex: 1,
              borderRadius: 10,
              fontFamily: 'Nunito-Regular',
              fontSize: 14,
              color: colors.pureBlack,
              paddingHorizontal: 15,
              paddingVertical: 8,
              marginRight: 5,
            }}
          />

          <TouchableOpacity onPress={onSend}>
            <SendSvg width={35} height={35} />
          </TouchableOpacity>
        </View>
      </KeyboardAvoidingView>

      {React.Children.toArray(
        responses.map((item) => {
          return (
            <Content.Response
              option={item.option}
              containerStyles={{marginLeft: 15, marginRight: 49}}
            />
          );
        }),
      )}
    </>
  );
};

const Response = ({toggle, option, onPress, containerStyles}) => {
  return (
    <View style={{marginBottom: 5}}>
      <View
        style={{
          borderTopWidth: toggle ? 1 : 0,
          borderTopColor: colors.grey,
          marginBottom: toggle ? 6 : 0,
          marginLeft: 15,
          marginRight: 11,
        }}
      />
      <TouchableOpacity
        onPress={onPress}
        style={[
          {
            borderColor: toggle ? colors.seagreen : colors.pureBlack,
            borderWidth: 1,
            borderRadius: 10,
            padding: 10,
            flexDirection: 'row',
          },
          containerStyles,
        ]}>
        <Button.TickToggle toggle={toggle} />

        <View style={{flex: 1, alignItems: 'center'}}>
          <TextTypes.RegularText
            style={{color: toggle ? colors.seagreen : colors.pureBlack}}>
            {option}
          </TextTypes.RegularText>
        </View>
      </TouchableOpacity>

      <View
        style={{
          borderTopWidth: toggle ? 1 : 0,
          borderTopColor: colors.grey,
          marginTop: toggle ? 4 : 0,
          marginRight: 11,
          marginLeft: 15,
        }}
      />
    </View>
  );
};

const SectionHeading = ({
  text,
  showButton = false,
  onPress,
  containerStyles,
  textStyles,
  buttonStyles,
}) => {
  return (
    <Row
      style={[
        {
          borderBottomColor: colors.lightGrey2,
          borderBottomWidth: 1,
          paddingBottom: 5.5,
        },
        containerStyles,
      ]}>
      <TextTypes.RegularText
        style={[{flex: 1, color: colors.black}, textStyles]}>
        {text}
      </TextTypes.RegularText>
      {showButton && (
        <TouchableOpacity style={{buttonStyles}} onPress={onPress}>
          <AddSvg />
        </TouchableOpacity>
      )}
    </Row>
  );
};

const Topic = ({
  text,
  backgroundColor,
  containerStyles,
  textStyles,
  showCross = false,
  onPress,
}) => {
  return (
    <Col
      style={[
        {
          backgroundColor,
          paddingVertical: 9,
          borderRadius: 5,
        },
        containerStyles,
      ]}>
      {showCross && (
        <TouchableOpacity
          onPress={onPress}
          style={{
            position: 'absolute',
            top: 0,
            right: 0,
            marginTop: -6,
            marginRight: -6,
          }}>
          <CrossSvg />
        </TouchableOpacity>
      )}

      <TextTypes.RegularText
        numberOfLines={1}
        style={[
          {
            fontFamily: 'Nunito-Regular',
            fontSize: 15,
            fontWeight: '400',
            textAlign: 'center',
          },
          textStyles,
        ]}>
        {text}
      </TextTypes.RegularText>
    </Col>
  );
};

const Content = {
  ArticleCard,
  DailyActivityCard,
  Message,
  MessageList,
  Response,
  SectionHeading,
  Topic,
};

export default Content;
