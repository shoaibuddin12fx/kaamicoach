import React from 'react';
import {Text, TextInput, View} from 'react-native';
import colors from '../../constants/color';

const Input = React.forwardRef(
  (
    {
      inputStyles,
      containerStyles,
      disabled = false,
      showError = false,
      errorText,
      ...props
    },
    ref,
  ) => {
    return (
      <View
        style={[
          {
            borderColor: colors.black,
            borderRadius: 10,
            shadowOpacity: 0.25,
            elevation: 20,
            shadowOffset: {
              width: 0,
              height: 5,
            },
          },
          containerStyles,
        ]}>
        <TextInput
          ref={ref}
          editable={!disabled}
          placeholderTextColor={colors.darkGrey2}
          style={[
            {
              fontFamily: 'Nunito-Regular',
              fontSize: 18,
              color: colors.black,
              paddingHorizontal: 18,
              paddingVertical: 12,
              backgroundColor: disabled ? colors.grey : colors.white,
              borderRadius: 10,
            },
            inputStyles,
          ]}
          {...props}
        />

        {showError && (
          <Text
            style={{
              fontFamily: 'Nunito-Semibold',
              fontSize: 12,
              fontWeight: '400',
              color: colors.red,
              marginTop: 5,
            }}>
            {errorText}
          </Text>
        )}
      </View>
    );
  },
);

export default Input;
