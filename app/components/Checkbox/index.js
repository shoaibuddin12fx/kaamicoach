import React from 'react';
import {TouchableOpacity} from 'react-native';
import colors from '../../constants/color';
import TickSvg from '../../assets/svg/tick.svg';

const Checkbox = ({toggle, onPress, containerStyles}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[
        {
          width: 14,
          height: 14,
          borderWidth: toggle ? 0 : 1,
          borderColor: colors.lightGrey,
          backgroundColor: toggle ? colors.seagreen : colors.white,
          alignItems: 'center',
          justifyContent: 'center',
        },
        containerStyles,
      ]}>
      {toggle && <TickSvg width={10} height={10} />}
    </TouchableOpacity>
  );
};

export default Checkbox;
