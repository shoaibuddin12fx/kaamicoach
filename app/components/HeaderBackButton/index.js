import React from 'react';
import LeftSvg from '../../assets/svg/components/header/LeftSvg';
import colors from '../../constants/color';
import TextTypes from '../Text';

const HeaderBackButton = ({text, onPress, svgContainerStyles}) => {
  return (
    <LeftSvg
      onPress={onPress}
      containerStyles={[
        {
          flexDirection: 'row',
          alignItems: 'center',
          marginBottom: 43,
        },
        svgContainerStyles,
      ]}
      style={{marginRight: 8}}
      customContent={
        <TextTypes.RegularText
          style={{
            color: colors.seagreen,
            fontSize: 14,
            fontFamily: 'Nunito-Regular',
            fontWeight: '400',
          }}>
          {text}
        </TextTypes.RegularText>
      }
    />
  );
};

export default HeaderBackButton;
