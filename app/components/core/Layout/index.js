import React from 'react';
import {View} from 'react-native';

export const Row = ({children, style, ...props}) => (
  <View
    style={[{flexDirection: 'row', alignItems: 'center'}, style]}
    {...props}>
    {children}
  </View>
);

export const Col = ({children, style, ...props}) => (
  <View style={[{flexDirection: 'column'}, style]} {...props}>
    {children}
  </View>
);
