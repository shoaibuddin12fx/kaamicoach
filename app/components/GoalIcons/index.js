import React from 'react';
import Walk from '../assets/svg/goal/walk.svg';
import Shuttle from '../assets/svg/goal/shuttle.svg';
import Flower from '../assets/svg/goal/flower.svg';
import Umbrella from '../assets/svg/goal/umbrella.svg';
import Breifcase from '../assets/svg/goal/breifcase.svg';
import Cup from '../assets/svg/goal/cup.svg';
import Child from '../assets/svg/goal/child.svg';
import Golf from '../assets/svg/goal/golf.svg';
import Woman from '../assets/svg/goal/woman.svg';
import Couple from '../assets/svg/goal/couple.svg';
import Sun from '../assets/svg/goal/sun.svg';
import Bike from '../assets/svg/goal/bike.svg';
import Trolley from '../assets/svg/goal/trolley.svg';
import Fitness from '../assets/svg/goal/fitness.svg';
import Spa from '../assets/svg/goal/spa.svg';
import Grocery from '../assets/svg/goal/grocery.svg';
import Unsatisfied from '../assets/svg/goal/unsatisfied.svg';
import Neutral from '../assets/svg/goal/neutral.svg';
import Satisfied from '../assets/svg/goal/satisfied.svg';
import Like from '../assets/svg/goal/like.svg';
import Sleep from '../assets/svg/goal/sleep.svg';
import Face from '../assets/svg/goal/face.svg';
import Camera from '../assets/svg/goal/camera.svg';
import Dining from '../assets/svg/goal/dining.svg';
import Pet from '../assets/svg/goal/pet.svg';
import Heart from '../assets/svg/goal/heart.svg';
import Horse from '../assets/svg/goal/horse.svg';
import Offer from '../assets/svg/goal/offer.svg';
import {Col, Row} from '../core/Layout';

const GoalIcons = ({containerStyle}) => {
  return (
    <Col style={[{marginHorizontal: 40}, containerStyle]}>
      <Row style={{justifyContent: 'space-around', marginBottom: 26}}>
        <Walk />
        <Shuttle />
        <Flower />
        <Umbrella />
        <Breifcase />
        <Cup />
        <Child />
      </Row>

      <Row style={{justifyContent: 'space-around', marginBottom: 26}}>
        <Golf />
        <Woman />
        <Couple />
        <Sun />
        <Bike />
        <Trolley />
        <Fitness />
      </Row>

      <Row style={{justifyContent: 'space-around', marginBottom: 26}}>
        <Spa />
        <Grocery />
        <Unsatisfied />
        <Neutral />
        <Satisfied />
        <Like />
        <Sleep />
      </Row>

      <Row style={{justifyContent: 'space-around'}}>
        <Face />
        <Camera />
        <Dining />
        <Pet />
        <Heart />
        <Horse />
        <Offer />
      </Row>
    </Col>
  );
};

export default GoalIcons;
