import React from 'react';
import {View} from 'react-native';
import colors from '../../constants/color';

const Avatar = () => {
  return (
    <View
      style={{
        width: 75,
        height: 75,
        borderRadius: 37.5,
        borderWidth: 3,
        borderColor: colors.lightGrey,
        backgroundColor: colors.grey,
      }}></View>
  );
};

export default Avatar;
