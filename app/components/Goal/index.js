import React from 'react';
import {TouchableOpacity} from 'react-native';
import Checkbox from '../Checkbox';
import colors from '../../constants/color';
import {Row} from '../core/Layout';
import GlowBlob from '../GlowBlob';
import TextTypes from '../Text';

const Goal = ({isActive, text, blobContent, containerStyles, onPress}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <Row style={containerStyles}>
        <GlowBlob
          containerStyles={{marginRight: 6}}
          customContent={blobContent}
          r={isActive ? 55 : 197}
          g={isActive ? 192 : 198}
          b={isActive ? 176 : 198}
        />

        <Row
          style={{
            borderWidth: isActive ? 1.5 : 0,
            borderColor: colors.seagreen,
            flex: 1,
            borderRadius: 10,
            paddingVertical: 12,
          }}>
          <TextTypes.RegularText
            style={{
              color: isActive ? colors.seagreen : colors.pureBlack,
              paddingLeft: 11,
              flex: 1,
              fontSize: 14,
              fontFamily: 'Nunito-Regular',
              fontWeight: '400',
            }}>
            {text}
          </TextTypes.RegularText>

          <Checkbox
            containerStyles={{
              borderColor: isActive ? colors.seagreen : colors.lightGrey,
              marginRight: 21,
            }}
          />
        </Row>
      </Row>
    </TouchableOpacity>
  );
};

export default Goal;
