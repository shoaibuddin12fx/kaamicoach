import types from '../types';

const INITIAL_STATE = {
  showIntro: true,
  showAuth: true,
  showInitialChat: false,
  initialAuthStackRoute: '',
  user: {},
  jwt: '',
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.NAVIGATE_FROM_INTRO:
      return {
        ...state,
        showIntro: false,
        initialAuthStackRoute: action.initialAuthStackRoute,
      };
    case types.ADD_USER:
      return {...state, showAuth: false, user: action.user, jwt: action.jwt};
    default:
      return state;
  }
};
