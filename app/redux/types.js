const NAVIGATE_FROM_INTRO = 'NAVIGATE_FROM_INTRO';

const ADD_USER = 'ADD_USER';

const types = {
  NAVIGATE_FROM_INTRO,
  ADD_USER,
};

export default types;
