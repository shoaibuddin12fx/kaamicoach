import React, {useCallback, useState} from 'react';
import {FlatList, TouchableOpacity} from 'react-native';
import colors from '../../constants/color';
import Content from '../../components/Content';
import Input from '../../components/Input';
import TextTypes from '../../components/Text';
import Family from './components/Family';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import moment from 'moment';
import Button from '../../components/Button';
import BottomTabView from '../../components/BottomTabView';

const EditFamily = ({navigation}) => {
  const [isDatePickerVisible, setIsDatePickerVisible] = useState(false);
  const [dob, setDob] = useState('');

  const family = [
    {
      initial: 'J',
      date: 'August 2017',
      r: 240,
      g: 128,
      b: 128,
    },
    {
      initial: 'B',
      date: 'August 2017',
      r: 168,
      g: 198,
      b: 134,
    },
    {
      initial: 'C',
      date: 'August 2017',
      r: 254,
      g: 219,
      b: 93,
    },
  ];

  const openDatePicker = useCallback(() => {
    setIsDatePickerVisible(true);
  }, [setIsDatePickerVisible]);

  const hideDatePicker = useCallback(() => {
    setIsDatePickerVisible(false);
  }, [setIsDatePickerVisible]);

  const updateDate = useCallback(
    (date) => {
      setIsDatePickerVisible(false);
      setDob(moment(date).format('MM/YYYY'));
    },
    [setIsDatePickerVisible, setDob],
  );

  return (
    <BottomTabView
      navigation={navigation}
      headerText="Profile"
      containterStyles={{flex: 1}}
      useScroll={false}>
      <FlatList
        bounces={false}
        data={family}
        keyboardShouldPersistTaps="handled"
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item, index}) => {
          return (
            <Family
              useEdit
              containterStyles={{
                marginBottom: index == family.length - 1 ? 34 : 18,
                marginHorizontal: 10,
              }}
              initial={item.initial}
              date={item.date}
              textStyles={{
                fontSize: 18,
                fontFamily: 'Nunito-Regular',
                fontWeight: '400',
                color: colors.darkGrey2,
              }}
              r={item.r}
              g={item.g}
              b={item.b}
            />
          );
        }}
        ListHeaderComponent={
          <Content.SectionHeading
            text="My Family"
            containerStyles={{marginBottom: 16, marginHorizontal: 10}}
          />
        }
        ListFooterComponent={
          <>
            <Content.SectionHeading
              text="New Family Member"
              containerStyles={{marginBottom: 13, marginHorizontal: 10}}
            />

            <Input
              inputStyles={{backgroundColor: colors.grey}}
              placeholder="Name"
              containerStyles={{marginBottom: 7, marginHorizontal: 10}}
            />

            <TextTypes.RegularText
              style={{
                color: colors.black,
                fontSize: 12,
                fontWeight: '300',
                fontFamily: 'Nunito-Regular',
                textAlign: 'right',
                marginBottom: 1,
              }}>
              Name
            </TextTypes.RegularText>

            <TouchableOpacity onPress={openDatePicker}>
              <Input
                pointerEvents="none"
                inputStyles={{backgroundColor: colors.grey}}
                placeholder="MM/YYYY"
                value={dob}
                containerStyles={{marginBottom: 7, marginHorizontal: 10}}
              />
            </TouchableOpacity>

            <TextTypes.RegularText
              style={{
                color: colors.black,
                fontSize: 12,
                fontWeight: '300',
                fontFamily: 'Nunito-Regular',
                textAlign: 'right',
              }}>
              D.O.B
            </TextTypes.RegularText>

            <Button.SaveButton containerStyles={{marginHorizontal: 10}} />
          </>
        }
      />

      <DateTimePickerModal
        isVisible={isDatePickerVisible}
        mode="date"
        onConfirm={updateDate}
        onCancel={hideDatePicker}
      />
    </BottomTabView>
  );
};

export default EditFamily;
