import React from 'react';
import DropDownPicker from 'react-native-dropdown-picker';
import BottomTabView from '../../components/BottomTabView';
import colors from '../../constants/color';
import Content from '../../components/Content';
import Input from '../../components/Input';

const EditGoal = ({navigation}) => {
  const frequencyList = [
    {
      label: 'Daily',
      value: 0,
    },
    {
      label: 'Weekly',
      value: 0,
    },
    {
      label: 'Monthly',
      value: 0,
    },
  ];

  return (
    <BottomTabView navigation={navigation} headerText="Profile">
      <Content.SectionHeading
        text="Edit / Delete Goal"
        containerStyles={{marginBottom: 13}}
      />

      <Input
        placeholder="Practice Relaxation Exercise"
        inputStyles={{backgroundColor: colors.grey}}
        containerStyles={{marginBottom: 31}}
      />

      <Content.SectionHeading
        text="Frequency"
        containerStyles={{marginBottom: 17}}
      />

      <DropDownPicker
        items={frequencyList}
        placeholder="Choose Frequency"
        placeholderStyle={{
          fontFamily: 'Nunito-Regular',
          fontSize: 18,
          fontWeight: '400',
          color: colors.darkGrey2,
        }}
        style={{
          backgroundColor: colors.grey,
          borderTopLeftRadius: 10,
          borderBottomLeftRadius: 10,
          borderTopRightRadius: 10,
          borderBottomRightRadius: 10,
          shadowOpacity: 0.25,
          elevation: 20,
          shadowOffset: {
            width: 0,
            height: 5,
          },
        }}
        containerStyle={{height: 50, marginBottom: 34}}
        zIndex={1}
        dropDownMaxHeight={90}
        itemStyle={{justifyContent: 'flex-start'}}
        selectedLabelStyle={{
          fontFamily: 'Nunito-Regular',
          fontSize: 18,
          fontWeight: '400',
        }}
        labelStyle={{
          alignSelf: 'center',
          fontFamily: 'Nunito-Regular',
          fontSize: 12,
          color: colors.pureBlack,
        }}
      />

      <Content.SectionHeading text="Icon" />
    </BottomTabView>
  );
};

export default EditGoal;
