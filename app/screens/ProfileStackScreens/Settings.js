import React from 'react';
import BottomTabView from '../../components/BottomTabView';
import Button from '../../components/Button';
import colors from '../../constants/color';
import Content from '../../components/Content';
import Input from '../../components/Input';
import TextTypes from '../../components/Text';

const Settings = ({navigation}) => {
  return (
    <BottomTabView navigation={navigation} headerText="Profile">
      <Content.SectionHeading
        text="Settings"
        containerStyles={{marginBottom: 25}}
      />

      <Input
        value="Eric Brodnock"
        inputStyles={{backgroundColor: colors.grey}}
        containerStyles={{marginBottom: 4}}
      />

      <TextTypes.RegularText
        style={{
          color: colors.black,
          fontSize: 12,
          fontWeight: '300',
          fontFamily: 'Nunito-Regular',
          textAlign: 'right',
          marginBottom: 4,
        }}>
        Name
      </TextTypes.RegularText>

      <Input
        value="ericbrodnock@gmail.com"
        inputStyles={{backgroundColor: colors.grey}}
        containerStyles={{marginBottom: 4}}
      />

      <TextTypes.RegularText
        style={{
          color: colors.black,
          fontSize: 12,
          fontWeight: '300',
          fontFamily: 'Nunito-Regular',
          textAlign: 'right',
          marginBottom: 4,
        }}>
        Email
      </TextTypes.RegularText>

      <Input
        value="1234567"
        secureTextEntry
        inputStyles={{backgroundColor: colors.grey, color: colors.lightGrey2}}
        containerStyles={{marginBottom: 7}}
      />

      <TextTypes.RegularText
        style={{
          color: colors.black,
          fontSize: 12,
          fontWeight: '300',
          fontFamily: 'Nunito-Regular',
          textAlign: 'right',
          marginBottom: 36,
        }}>
        Password
      </TextTypes.RegularText>

      <Button.SaveButton />
    </BottomTabView>
  );
};

export default Settings;
