import React, {useCallback, useEffect} from 'react';
import {FlatList, SafeAreaView, TouchableOpacity} from 'react-native';
import SettingSvg from '../../assets/svg/components/header/SettingSvg';
import colors from '../../constants/color';
import Content from '../../components/Content';
import {Col} from '../../components/core/Layout';
import TextTypes from '../../components/Text';
import Family from './components/Family';
import Goal from '../../components/Goal';

const ProfileScreen = ({navigation, route}) => {
  useEffect(() => {
    if (route.params && route.params.screen) {
      navigation.navigate(route.params.screen);
    }
  });

  const goals = [
    {
      title: 'Practice Relexation Techniques',
      active: true,
    },
    {
      title: 'Go for a walk',
      active: false,
    },
    {
      title: 'Journal',
      active: false,
    },
  ];

  const family = [
    {
      initial: 'J',
      date: 'August 2017',
      r: 240,
      g: 128,
      b: 128,
    },
    {
      initial: 'B',
      date: 'August 2017',
      r: 168,
      g: 198,
      b: 134,
    },
    {
      initial: 'C',
      date: 'August 2017',
      r: 254,
      g: 219,
      b: 93,
    },
  ];

  const navigateToAddGoal = useCallback(() => {
    navigation.navigate('AddGoal');
  }, [navigation]);

  const navigateToEditGoal = useCallback(() => {
    navigation.navigate('EditGoal');
  }, [navigation]);

  const navigateToSettings = useCallback(() => {
    navigation.navigate('Settings');
  }, [navigation]);

  const navigateToEditFamily = useCallback(() => {
    navigation.navigate('EditFamily');
  }, [navigation]);

  return (
    <SafeAreaView style={{backgroundColor: colors.white, flexGrow: 1}}>
      <TouchableOpacity onPress={navigateToSettings}>
        <SettingSvg
          width={22}
          height={22}
          style={{alignSelf: 'flex-end', marginRight: 36}}
        />
      </TouchableOpacity>

      <Col style={{marginHorizontal: 25}}>
        <Col
          style={{
            width: 75,
            height: 75,
            borderRadius: 37.5,
            backgroundColor: colors.grey,
            alignSelf: 'center',
            marginBottom: 12,
          }}
        />

        <TextTypes.RegularText
          style={{color: colors.black, alignSelf: 'center', marginBottom: 12}}>
          Eric Brodnock
        </TextTypes.RegularText>

        <FlatList
          bounces={false}
          contentContainerStyle={{marginBottom: 26}}
          data={goals}
          keyExtractor={(item, index) => index.toString()}
          ItemSeparatorComponent={() => (
            <Col
              style={{
                width: 1,
                height: 18,
                backgroundColor: colors.lightGrey,
                marginLeft: 27.5,
              }}
            />
          )}
          renderItem={({item}) => {
            return (
              <Goal
                isActive={item.active}
                text={item.title}
                onPress={navigateToEditGoal}
              />
            );
          }}
          ListHeaderComponent={
            <Content.SectionHeading
              onPress={navigateToAddGoal}
              text="My Goals"
              showButton
              containerStyles={{marginBottom: 16}}
            />
          }
        />

        <FlatList
          bounces={false}
          data={family}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item}) => {
            return (
              <Family
                containterStyles={{marginBottom: 18}}
                initial={item.initial}
                date={item.date}
                textStyles={{
                  marginLeft: 16,
                  fontSize: 14,
                  fontFamily: 'Nunito-Regular',
                  fontWeight: '400',
                }}
                r={item.r}
                g={item.g}
                b={item.b}
              />
            );
          }}
          ListHeaderComponent={
            <Content.SectionHeading
              text="My Family"
              showButton
              containerStyles={{marginBottom: 16}}
              onPress={navigateToEditFamily}
            />
          }
        />
      </Col>
    </SafeAreaView>
  );
};

export default ProfileScreen;
