import React from 'react';
import {TouchableOpacity} from 'react-native';
import CrossSvg from '../../../assets/svg/components/additional/CrossSvg';
import colors from '../../../constants/color';
import {Col, Row} from '../../../components/core/Layout';
import GlowBlob from '../../../components/GlowBlob';
import TextTypes from '../../../components/Text';

const Family = ({
  initial,
  date,
  containterStyles,
  textStyles,
  r,
  g,
  b,
  useEdit = false,
  onCrossPress,
}) => {
  return (
    <Row style={containterStyles}>
      <GlowBlob text={initial} r={r} g={g} b={b} />

      {useEdit ? (
        <Col
          style={{
            backgroundColor: colors.grey,
            marginLeft: 10,
            flex: 1,
            paddingVertical: 14,
            paddingHorizontal: 16,
            borderRadius: 10,
          }}>
          <TouchableOpacity
            onPress={onCrossPress}
            style={{position: 'absolute', top: 0, right: 0}}>
            <CrossSvg />
          </TouchableOpacity>
          <TextTypes.RegularText
            style={[{color: colors.pureBlack}, textStyles]}>
            {date}
          </TextTypes.RegularText>
        </Col>
      ) : (
        <TextTypes.RegularText style={[{color: colors.pureBlack}, textStyles]}>
          {date}
        </TextTypes.RegularText>
      )}
    </Row>
  );
};

export default Family;
