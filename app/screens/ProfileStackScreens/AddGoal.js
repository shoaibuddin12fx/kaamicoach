import React from 'react';
import colors from '../../constants/color';
import {Col} from '../../components/core/Layout';
import Content from '../../components/Content';
import Input from '../../components/Input';
import DropDownPicker from 'react-native-dropdown-picker';
import BottomTabView from '../../components/BottomTabView';

const AddGoal = ({navigation}) => {
  const colorList = [
    {
      label: 'Yellow',
      value: '#FFF200',
      icon: () => (
        <Col
          style={{
            width: 27,
            height: 27,
            backgroundColor: '#FFF200',
            borderRadius: 10,
          }}
        />
      ),
    },
    {
      label: 'Red',
      value: '#D30000',
      icon: () => (
        <Col
          style={{
            width: 27,
            height: 27,
            backgroundColor: '#D30000',
            borderRadius: 10,
          }}
        />
      ),
    },
    {
      label: 'Pink',
      value: '#FC0FC0',
      icon: () => (
        <Col
          style={{
            width: 27,
            height: 27,
            backgroundColor: '#FC0FC0',
            borderRadius: 10,
          }}
        />
      ),
    },
    {
      label: 'Voilet',
      value: '#B200ED',
      icon: () => (
        <Col
          style={{
            width: 27,
            height: 27,
            backgroundColor: '#B200ED',
            borderRadius: 10,
          }}
        />
      ),
    },
    {
      label: 'Blue',
      value: '#0018F9',
      icon: () => (
        <Col
          style={{
            width: 27,
            height: 27,
            backgroundColor: '#0018F9',
            borderRadius: 10,
          }}
        />
      ),
    },
    {
      label: 'Green',
      value: '#3BB143',
      icon: () => (
        <Col
          style={{
            width: 27,
            height: 27,
            backgroundColor: '#3BB143',
            borderRadius: 10,
          }}
        />
      ),
    },
    {
      label: 'Brown',
      value: '#7C4700',
      icon: () => (
        <Col
          style={{
            width: 27,
            height: 27,
            backgroundColor: '#7C4700',
            borderRadius: 10,
          }}
        />
      ),
    },
    {
      label: 'Grey',
      value: '#828282',
      icon: () => (
        <Col
          style={{
            width: 27,
            height: 27,
            backgroundColor: '#828282',
            borderRadius: 10,
          }}
        />
      ),
    },
    {
      label: 'Orange',
      value: '#FC6600',
      icon: () => (
        <Col
          style={{
            width: 27,
            height: 27,
            backgroundColor: '#FC6600',
            borderRadius: 10,
          }}
        />
      ),
    },
  ];

  const frequencyList = [
    {
      label: 'Daily',
      value: 0,
    },
    {
      label: 'Weekly',
      value: 0,
    },
    {
      label: 'Monthly',
      value: 0,
    },
  ];

  return (
    <BottomTabView navigation={navigation} headerText="Profile">
      <Content.SectionHeading
        text="New Goal"
        containerStyles={{marginBottom: 13}}
      />

      <Input
        inputStyles={{backgroundColor: colors.grey}}
        placeholder="Title"
        containerStyles={{marginBottom: 30}}
      />

      <Content.SectionHeading
        text="Colour"
        containerStyles={{marginBottom: 14}}
      />

      <DropDownPicker
        items={colorList}
        //   onChangeItem={(item) => console.log({item})}
        placeholder="Choose color"
        placeholderStyle={{
          fontFamily: 'Nunito-Regular',
          fontSize: 18,
          fontWeight: '400',
          color: colors.darkGrey2,
        }}
        style={{
          backgroundColor: colors.grey,
          borderTopLeftRadius: 10,
          borderBottomLeftRadius: 10,
          borderTopRightRadius: 10,
          borderBottomRightRadius: 10,
          shadowOpacity: 0.25,
          elevation: 20,
          shadowOffset: {
            width: 0,
            height: 5,
          },
        }}
        containerStyle={{height: 50, marginBottom: 31}}
        zIndex={1}
        dropDownMaxHeight={90}
        itemStyle={{justifyContent: 'flex-start'}}
        selectedLabelStyle={{
          fontFamily: 'Nunito-Regular',
          fontSize: 18,
          fontWeight: '400',
          marginLeft: 9,
        }}
        labelStyle={{
          alignSelf: 'center',
          fontFamily: 'Nunito-Regular',
          fontSize: 12,
          color: colors.pureBlack,
          marginLeft: 9,
        }}
      />

      <Content.SectionHeading
        text="Frequency"
        containerStyles={{marginBottom: 17}}
      />

      <DropDownPicker
        items={frequencyList}
        placeholder="Choose Frequency"
        placeholderStyle={{
          fontFamily: 'Nunito-Regular',
          fontSize: 18,
          fontWeight: '400',
          color: colors.darkGrey2,
        }}
        style={{
          backgroundColor: colors.grey,
          borderTopLeftRadius: 10,
          borderBottomLeftRadius: 10,
          borderTopRightRadius: 10,
          borderBottomRightRadius: 10,
          shadowOpacity: 0.25,
          elevation: 20,
          shadowOffset: {
            width: 0,
            height: 5,
          },
        }}
        containerStyle={{height: 50, marginBottom: 34}}
        zIndex={1}
        dropDownMaxHeight={90}
        itemStyle={{justifyContent: 'flex-start'}}
        selectedLabelStyle={{
          fontFamily: 'Nunito-Regular',
          fontSize: 18,
          fontWeight: '400',
        }}
        labelStyle={{
          alignSelf: 'center',
          fontFamily: 'Nunito-Regular',
          fontSize: 12,
          color: colors.pureBlack,
        }}
      />

      <Content.SectionHeading
        text="Icon"
        containerStyles={{marginBottom: 17}}
      />
    </BottomTabView>
  );
};

export default AddGoal;
