import React, {useCallback} from 'react';
import {SafeAreaView, SectionList} from 'react-native';
import LogoSvg from '../../assets/svg/logo.svg';
import Button from '../../components/Button';
import colors from '../../constants/color';
import Content from '../../components/Content';
import {Col} from '../../components/core/Layout';
import Goal from '../../components/Goal';
import DailyActivity from './components/DailyActivity';

const DashboardScreen = ({navigation}) => {
  const list = [
    {
      title: "Today's Goals",
      data: [
        {
          title: 'Practice Relexation Techniques',
          active: true,
        },
        {
          title: 'Go for a walk',
          active: false,
        },
        {
          title: 'Journal',
          active: false,
        },
      ],
    },
    {
      title: 'Daily Activities',
      data: [
        {
          activityNumber: 'Three',
          title: 'Why is Bonding Important?',
          description:
            'The most important stage for brain development is the beginning of life, starting in the womb and then the first year of life...',
        },
      ],
    },
    {
      title: "Erika's Daily Reading",
      data: [
        {
          image: 'https://picsum.photos/seed/picsum/300/200',
          author: 'Patrick A. Coleman',
          status: 'Parent',
          title: 'How to Turn Happry Childresn Into Happy Adults',
          description:
            'Being the parent of a kid who "has it all" and yet no happiness to show for it is all too common....',
        },
        {
          image: 'https://picsum.photos/seed/picsum/300/200',
          author: 'Rachel Smith',
          status: 'Midwife at Barkantine Birth Centre',
          title: 'Why Peer Support is Vital',
        },
        {
          image: 'https://picsum.photos/seed/picsum/300/200',
          author: 'Rachel Smith',
          status: 'Midwife at Barkantine Birth Centre',
          title: 'Why Peer Support is Vital',
        },
      ],
    },
  ];

  const navigateToEditGoal = useCallback(() => {
    navigation.navigate('ProfileTab', {
      screen: 'Profile',
      params: {screen: 'EditGoal'},
    });
  }, [navigation]);

  const navigateToActivity = useCallback(() => {
    navigation.navigate('Activity');
  }, [navigation]);

  const navigateToArticle = useCallback(() => {
    navigation.navigate('NewsfeedTab', {
      screen: 'Newsfeed',
      params: {screen: 'Article'},
    });
  }, [navigation]);

  return (
    <SafeAreaView style={{backgroundColor: colors.white, flexGrow: 1, flex: 1}}>
      <Col style={{flex: 1, paddingHorizontal: 25}}>
        <LogoSvg width={41} height={49} style={{marginBottom: 34}} />

        <Button.NormalButton
          text="How can we help you today?"
          containerStyle={{marginHorizontal: 38, marginBottom: 35}}
        />
        <SectionList
          sections={list}
          keyExtractor={(item, index) => item + index}
          stickySectionHeadersEnabled={false}
          stickyHeaderIndices={[0]}
          showsVerticalScrollIndicator={false}
          renderSectionHeader={({section: {title}}) => {
            return (
              <Content.SectionHeading
                text={title}
                containerStyles={{marginBottom: 13}}
              />
            );
          }}
          ItemSeparatorComponent={({section: {title}}) => {
            switch (title) {
              case "Today's Goals":
                return (
                  <Col
                    style={{
                      width: 1,
                      height: 18,
                      backgroundColor: colors.lightGrey,
                      marginLeft: 27.5,
                    }}
                  />
                );
              default:
                return null;
            }
          }}
          renderItem={({item, index, section}) => {
            switch (section.title) {
              case "Today's Goals":
                return (
                  <Goal
                    isActive={item.active}
                    text={item.title}
                    containerStyles={{
                      marginBottom: section.data.length == index + 1 ? 25 : 0,
                    }}
                    onPress={navigateToEditGoal}
                  />
                );
              case 'Daily Activities':
                return (
                  <DailyActivity
                    activityNumber={item.activityNumber}
                    title={item.title}
                    description={item.description}
                    onPress={navigateToActivity}
                    containerStyles={{
                      marginBottom: section.data.length == index + 1 ? 30 : 10,
                    }}
                  />
                );
              case "Erika's Daily Reading":
                return (
                  <Content.ArticleCard
                    author={item.author}
                    status={item.status}
                    title={item.title}
                    description={item.description}
                    image={item.image}
                    containerStyles={{marginBottom: 36}}
                    onPress={navigateToArticle}
                  />
                );
              default:
                return null;
            }
          }}
        />
      </Col>
    </SafeAreaView>
  );
};

export default DashboardScreen;
