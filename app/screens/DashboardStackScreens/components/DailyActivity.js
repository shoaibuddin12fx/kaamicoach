import React from 'react';
import RightSvg from '../../../assets/svg/components/RightSvg';
import Button from '../../../components/Button';
import colors from '../../../constants/color';
import {Col, Row} from '../../../components/core/Layout';
import TextTypes from '../../../components/Text';

const DailyActivity = ({
  activityNumber,
  title,
  description,
  containerStyles,
  onPress,
}) => {
  return (
    <Col
      style={[
        {
          borderWidth: 1,
          borderColor: colors.seagreen,
          paddingHorizontal: 16,
          paddingVertical: 20,
          borderRadius: 10,
          backgroundColor: colors.white,
          shadowOpacity: 0.25,
          elevation: 20,
          shadowOffset: {
            width: 0,
            height: 5,
          },
        },
        containerStyles,
      ]}>
      <TextTypes.RegularText
        style={{
          fontSize: 14,
          fontFamily: 'Nunito-Regular',
          fontWeight: '400',
          color: colors.pureBlack,
          marginBottom: 8,
        }}>
        Activity {activityNumber}
      </TextTypes.RegularText>

      <TextTypes.RegularText
        style={{
          fontFamily: 'Nunito-Regular',
          fontWeight: '400',
          color: colors.pureBlack,
          marginBottom: 8,
        }}>
        {title}
      </TextTypes.RegularText>

      <TextTypes.RegularText
        style={{
          fontSize: 14,
          fontFamily: 'Nunito-Regular',
          fontWeight: '400',
          color: colors.pureBlack,
          marginBottom: 14,
        }}>
        {description}
      </TextTypes.RegularText>

      <Button.NormalButton
        onPress={onPress}
        customContent={
          <Row>
            <TextTypes.RegularText>Complete Activity</TextTypes.RegularText>

            <RightSvg color={colors.white} style={{marginLeft: 5}} />
          </Row>
        }
      />
    </Col>
  );
};

export default DailyActivity;
