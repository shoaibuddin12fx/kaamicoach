import React, {useCallback} from 'react';
import {
  Keyboard,
  SafeAreaView,
  ScrollView,
  TouchableHighlight,
} from 'react-native';
import colors from '../../../constants/color';
import {Col} from '../../../components/core/Layout';
import LogoSvg from '../../../assets/svg/logo.svg';

const AuthView = ({children}) => {
  const hideKeyboard = useCallback(() => {
    Keyboard.dismiss();
  }, [Keyboard]);

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: colors.white}}>
      <ScrollView
        contentContainerStyle={{flexGrow: 1}}
        keyboardShouldPersistTaps="handled">
        <TouchableHighlight
          underlayColor=""
          onPress={hideKeyboard}
          style={{flex: 1}}>
          <>
            <Col style={{alignItems: 'center', paddingVertical: 32}}>
              <LogoSvg />
            </Col>

            {children}
          </>
        </TouchableHighlight>
      </ScrollView>
    </SafeAreaView>
  );
};

export default AuthView;
