import React from 'react';
import Button from '../../components/Button';
import colors from '../../constants/color';
import {Col} from '../../components/core/Layout';
import TextTypes from '../../components/Text';
import AuthView from './components/AuthView';

const EmailVerifiedScreen = () => {
  return (
    <AuthView>
      <Col
        style={{
          flex: 1,
          backgroundColor: colors.lightBlue,
          borderTopLeftRadius: 100,
        }}>
        <TextTypes.SemiBoldText
          style={{
            color: colors.pureBlack,
            fontSize: 28,
            fontFamily: 'Nunito-Semibold',
            marginBottom: 32,
            textAlign: 'center',
            marginTop: 77,
          }}>
          Email Verified!
        </TextTypes.SemiBoldText>

        <TextTypes.RegularText
          style={{
            color: colors.black2,
            fontSize: 12,
            fontFamily: 'Nunito-Regular',
            textAlign: 'center',
            fontWeight: '300',
            marginBottom: 60,
          }}>
          You have successfully verified your email.
        </TextTypes.RegularText>

        <Button.NormalButton
          text="Get Started"
          containerStyle={{marginHorizontal: 64, marginBottom: 14}}
          onPress={addUser}
        />
      </Col>
    </AuthView>
  );
};

export default EmailVerifiedScreen;
