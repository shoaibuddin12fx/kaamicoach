import React, {useCallback} from 'react';
import {Keyboard, TouchableOpacity} from 'react-native';
import Button from '../../components/Button';
import colors from '../../constants/color';
import {Col} from '../../components/core/Layout';
import TextTypes from '../../components/Text';
import AuthView from './components/AuthView';
import CodeInput from 'react-native-confirmation-code-input';

const EnterCodeScreen = ({navigation}) => {
  const navigateToLogin = useCallback(() => {
    navigation.navigate('Login');
  }, [navigation]);

  const onFulfill = useCallback(() => {
    Keyboard.dismiss();
  }, [Keyboard]);

  return (
    <AuthView>
      <Col
        style={{
          flex: 1,
          backgroundColor: colors.lightBlue,
          borderTopLeftRadius: 100,
        }}>
        <TextTypes.SemiBoldText
          style={{
            color: colors.pureBlack,
            fontSize: 28,
            fontFamily: 'Nunito-Semibold',
            marginBottom: 38,
            textAlign: 'center',
            marginTop: 77,
          }}>
          Enter Code
        </TextTypes.SemiBoldText>

        <TextTypes.RegularText
          style={{
            color: colors.black2,
            fontSize: 12,
            fontFamily: 'Nunito-Regular',
            textAlign: 'center',
            fontWeight: '300',
            marginHorizontal: 60,
            marginBottom: 32,
          }}>
          A verification code has been sent to{' '}
          <TextTypes.RegularText
            style={{
              color: colors.black2,
              fontSize: 12,
              fontFamily: 'Nunito-Regular',
              fontWeight: '700',
            }}>
            hello@kami.coach
          </TextTypes.RegularText>
          {'\n'}Please enter it below.
        </TextTypes.RegularText>

        <CodeInput
          autoFocus={false}
          activeColor="#428AF8"
          inactiveColor="#428AF8"
          cellBorderWidth={0}
          inputPosition="center"
          size={55}
          containerStyle={{
            flex: 0,
            marginBottom: 10,
            shadowOpacity: 0.25,
            elevation: 20,
            shadowOffset: {
              width: 0,
              height: 5,
            },
          }}
          codeInputStyle={{
            width: 55,
            height: 50,
            backgroundColor: colors.white,
            borderRadius: 10,
            color: colors.pureBlack,
            fontFamily: 'Nunito-Regular',
            fontSize: 18,
          }}
          codeLength={4}
          space={19}
          onFulfill={onFulfill}
        />

        <TextTypes.RegularText
          style={{
            color: colors.black2,
            fontSize: 12,
            fontFamily: 'Nunito-Regular',
            textAlign: 'right',
            fontWeight: '300',
            marginRight: 72,
            marginBottom: 20,
          }}>
          Resend Code.
        </TextTypes.RegularText>

        <Button.NormalButton
          text="Confirm Email"
          containerStyle={{marginHorizontal: 64, marginBottom: 14}}
        />

        <TouchableOpacity onPress={navigateToLogin}>
          <TextTypes.RegularText
            style={{
              color: colors.black2,
              fontSize: 12,
              fontFamily: 'Nunito-Regular',
              textAlign: 'center',
              fontWeight: '300',
            }}>
            Already have an account? Log In.
          </TextTypes.RegularText>
        </TouchableOpacity>
      </Col>
    </AuthView>
  );
};

export default EnterCodeScreen;
