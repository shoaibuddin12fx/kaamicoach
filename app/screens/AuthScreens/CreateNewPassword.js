import React, {useCallback, useRef} from 'react';
import {TouchableOpacity} from 'react-native';
import Button from '../../components/Button';
import colors from '../../constants/color';
import {Col} from '../../components/core/Layout';
import Input from '../../components/Input';
import TextTypes from '../../components/Text';
import AuthView from './components/AuthView';

const CreateNewPasswordScreen = ({navigation}) => {
  const passwordInputRef = useRef(null);
  const rePasswordInputRef = useRef(null);

  const focusRePassword = useCallback(() => {
    if (rePasswordInputRef.current) {
      rePasswordInputRef.current.focus();
    }
  }, [rePasswordInputRef]);

  const navigateToLogin = useCallback(() => {
    navigation.navigate('Login');
  }, [navigation]);

  return (
    <AuthView>
      <Col
        style={{
          flex: 1,
          backgroundColor: colors.lightBlue,
          borderTopLeftRadius: 100,
        }}>
        <TextTypes.SemiBoldText
          style={{
            color: colors.pureBlack,
            fontSize: 28,
            fontFamily: 'Nunito-Semibold',
            marginBottom: 44,
            textAlign: 'center',
            marginTop: 77,
          }}>
          Create New Password
        </TextTypes.SemiBoldText>

        <Input
          ref={passwordInputRef}
          containerStyles={{marginHorizontal: 64, marginBottom: 9}}
          placeholder="Create Password"
          autoCorrect={false}
          textContentType="password"
          autoCapitalize="none"
          autoCompleteType="password"
          returnKeyType="next"
          onSubmitEditing={focusRePassword}
        />

        <Input
          ref={rePasswordInputRef}
          containerStyles={{marginHorizontal: 64, marginBottom: 22}}
          placeholder="Re-enter Password"
          autoCorrect={false}
          textContentType="password"
          autoCapitalize="none"
          autoCompleteType="password"
          returnKeyType="go"
        />

        <Button.NormalButton
          text="Confirm"
          containerStyle={{marginHorizontal: 64, marginBottom: 14}}
        />

        <TouchableOpacity onPress={navigateToLogin}>
          <TextTypes.RegularText
            style={{
              color: colors.black2,
              fontSize: 12,
              fontFamily: 'Nunito-Regular',
              textAlign: 'center',
            }}>
            Know your password? Log In.
          </TextTypes.RegularText>
        </TouchableOpacity>
      </Col>
    </AuthView>
  );
};

export default CreateNewPasswordScreen;
