import React, {useCallback, useRef} from 'react';
import {TouchableOpacity} from 'react-native';
import Button from '../../components/Button';
import colors from '../../constants/color';
import {Col} from '../../components/core/Layout';
import Input from '../../components/Input';
import TextTypes from '../../components/Text';
import AuthView from './components/AuthView';

const ForgotPasswordScreen = ({navigation}) => {
  const emailInputRef = useRef(null);

  const navigateToLogin = useCallback(() => {
    navigation.navigate('Login');
  }, [navigation]);

  const validate = useCallback(() => {
    navigation.navigate('EnterCode');
  }, [navigation]);

  return (
    <AuthView>
      <Col
        style={{
          flex: 1,
          backgroundColor: colors.lightBlue,
          borderTopLeftRadius: 100,
        }}>
        <TextTypes.SemiBoldText
          style={{
            color: colors.pureBlack,
            fontSize: 28,
            fontFamily: 'Nunito-Semibold',
            marginBottom: 44,
            textAlign: 'center',
            marginTop: 77,
          }}>
          Forgot Password?
        </TextTypes.SemiBoldText>

        <Input
          ref={emailInputRef}
          containerStyles={{marginHorizontal: 64, marginBottom: 21}}
          placeholder="Email"
          autoCorrect={false}
          autoCompleteType="email"
          autoCapitalize="none"
          textContentType="emailAddress"
          keyboardType="email-address"
          returnKeyType="go"
          // onSubmitEditing={validate}
        />

        <Button.NormalButton
          text="Forgot Password"
          containerStyle={{marginHorizontal: 64, marginBottom: 14}}
          onPress={validate}
        />

        <TouchableOpacity onPress={navigateToLogin}>
          <TextTypes.RegularText
            style={{
              color: colors.black2,
              fontSize: 12,
              fontFamily: 'Nunito-Regular',
              textAlign: 'center',
            }}>
            Know your password? Log In.
          </TextTypes.RegularText>
        </TouchableOpacity>
      </Col>
    </AuthView>
  );
};

export default ForgotPasswordScreen;
