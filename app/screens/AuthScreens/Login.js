import React, {useCallback, useRef, useState} from 'react';
import {Keyboard, TouchableOpacity} from 'react-native';
import Button from '../../components/Button';
import colors from '../../constants/color';
import {Col} from '../../components/core/Layout';
import Input from '../../components/Input';
import TextTypes from '../../components/Text';
import AuthView from './components/AuthView';
import Api from '../../api';
import {emailRegex} from '../../constants/regex';
import {useDispatch} from 'react-redux';
import types from '../../redux/types';

const LoginScreen = ({navigation}) => {
  const dispatch = useDispatch();

  const [email, setEmail] = useState('');
  const [validEmail, setValidEmail] = useState(true);
  const [password, setPassword] = useState('');
  const [validPassword, setValidPassword] = useState(true);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);

  const emailInputRef = useRef(null);
  const passwordInputRef = useRef(null);

  const onChangeEmail = useCallback(
    (email) => {
      if (!validEmail) setValidEmail(true);
      if (error) setError(false);
      setEmail(email);
    },
    [validEmail, error, setValidEmail, setEmail],
  );

  const onChangePassword = useCallback(
    (password) => {
      if (!validPassword) setValidPassword(true);
      if (error) setError(false);
      setPassword(password);
    },
    [validPassword, error, setValidPassword, setPassword],
  );

  const validate = useCallback(async () => {
    Keyboard.dismiss();
    if (error) setError(false);
    setLoading(true);
    let valid = true;

    if (!emailRegex.test(email)) {
      valid = false;
      setValidEmail(false);
    }

    if (password.length < 7) {
      valid = false;
      setValidPassword(false);
    }

    if (valid) {
      let data = new FormData();
      data.append('identifier', email);
      data.append('password', password);

      try {
        let res = await Api.post('/auth/local', data);
        setLoading(false);

        console.log('User login Api', res);

        dispatch({
          type: types.ADD_USER,
          jwt: res.data.jwt,
          user: res.data.user,
        });
      } catch (err) {
        console.log({err});
        setError(true);
        setLoading(false);
      }
    } else {
      setLoading(false);
    }
  }, [
    setValidEmail,
    setValidPassword,
    error,
    email,
    password,
    setLoading,
    Keyboard,
    setError,
    dispatch,
  ]);

  const navigateToForgotPassword = useCallback(() => {
    navigation.navigate('ForgotPassword');
  }, [navigation]);

  const navigateToSignUp = useCallback(() => {
    navigation.navigate('SignUp');
  }, [navigation]);

  const focusPassword = useCallback(() => {
    if (passwordInputRef.current) {
      passwordInputRef.current.focus();
    }
  }, [passwordInputRef]);

  return (
    <AuthView>
      <Col
        style={{
          flex: 1,
          backgroundColor: colors.lightBlue,
          borderTopLeftRadius: 100,
        }}>
        <TextTypes.SemiBoldText
          style={{
            color: colors.pureBlack,
            fontSize: 28,
            fontFamily: 'Nunito-Semibold',
            marginBottom: 44,
            textAlign: 'center',
            marginTop: 77,
          }}>
          Login
        </TextTypes.SemiBoldText>

        <Input
          editable={!loading}
          ref={emailInputRef}
          containerStyles={{marginHorizontal: 64, marginBottom: 9}}
          placeholder="Email"
          autoCorrect={false}
          autoCompleteType="email"
          autoCapitalize="none"
          textContentType="emailAddress"
          keyboardType="email-address"
          returnKeyType="next"
          value={email}
          onChangeText={onChangeEmail}
          onSubmitEditing={focusPassword}
          showError={!validEmail}
          errorText="Email is not valid. Please try again"
        />

        <Input
          editable={!loading}
          ref={passwordInputRef}
          secureTextEntry
          containerStyles={{marginHorizontal: 64, marginBottom: 9}}
          placeholder="Password"
          autoCorrect={false}
          textContentType="password"
          autoCapitalize="none"
          autoCompleteType="password"
          returnKeyType="go"
          value={password}
          onChangeText={onChangePassword}
          onSubmitEditing={validate}
          showError={!validPassword}
          errorText="Length of password must be seven"
        />

        <TouchableOpacity
          style={{
            flexWrap: 'wrap',
            alignSelf: 'flex-end',
            marginRight: 64,
            marginBottom: 24,
          }}
          onPress={navigateToForgotPassword}>
          <TextTypes.RegularText
            style={{
              color: colors.black2,
              fontSize: 12,
              fontFamily: 'Nunito-Regular',
              flexWrap: 'wrap',
            }}>
            Forgot Password?
          </TextTypes.RegularText>
        </TouchableOpacity>

        <Button.NormalButton
          text="Log In"
          containerStyle={{marginHorizontal: 64, marginBottom: 14}}
          onPress={validate}
          showLoading={loading}
        />

        <TouchableOpacity
          style={{flexWrap: 'wrap', alignSelf: 'center'}}
          onPress={navigateToSignUp}>
          <TextTypes.RegularText
            style={{
              color: colors.black2,
              fontSize: 12,
              fontFamily: 'Nunito-Regular',
            }}>
            Don’t have an account? Sign Up.
          </TextTypes.RegularText>
        </TouchableOpacity>

        {error && (
          <TextTypes.RegularText
            style={{
              color: colors.red,
              fontSize: 12,
              fontFamily: 'Nunito-Semibold',
              textAlign: 'center',
              marginTop: 50,
            }}>
            Something went wrong. Please try again.
          </TextTypes.RegularText>
        )}
      </Col>
    </AuthView>
  );
};

export default LoginScreen;
