import React, {useCallback, useRef} from 'react';
import {TouchableOpacity} from 'react-native';
import Button from '../../components/Button';
import colors from '../../constants/color';
import {Col} from '../../components/core/Layout';
import Input from '../../components/Input';
import TextTypes from '../../components/Text';
import AuthView from './components/AuthView';

const SignUpScreen = ({navigation}) => {
  const nameInputRef = useRef(null);
  const emailInputRef = useRef(null);
  const passwordInputRef = useRef(null);
  const rePasswordInputRef = useRef(null);

  const focusEmail = useCallback(() => {
    if (emailInputRef.current) {
      emailInputRef.current.focus();
    }
  }, [emailInputRef]);

  const focusPassword = useCallback(() => {
    if (passwordInputRef.current) {
      passwordInputRef.current.focus();
    }
  }, [passwordInputRef]);

  const focusRePassword = useCallback(() => {
    if (rePasswordInputRef.current) {
      rePasswordInputRef.current.focus();
    }
  }, [rePasswordInputRef]);

  const navigateToLogin = useCallback(() => {
    navigation.navigate('Login');
  }, [navigation]);

  const navigateToConfirmEmail = useCallback(() => {
    navigation.navigate('ConfirmEmail');
  }, [navigation]);

  return (
    <AuthView>
      <Col
        style={{
          flex: 1,
          backgroundColor: colors.lightBlue,
          borderTopLeftRadius: 100,
        }}>
        <TextTypes.SemiBoldText
          style={{
            color: colors.pureBlack,
            fontSize: 28,
            fontFamily: 'Nunito-Semibold',
            marginBottom: 44,
            textAlign: 'center',
            marginTop: 77,
          }}>
          Sign Up
        </TextTypes.SemiBoldText>

        <Input
          ref={nameInputRef}
          containerStyles={{marginHorizontal: 64, marginBottom: 9}}
          placeholder="Name"
          autoCorrect={false}
          autoCompleteType="name"
          autoCapitalize="words"
          textContentType="name"
          keyboardType="default"
          returnKeyType="next"
          onSubmitEditing={focusEmail}
        />

        <Input
          ref={emailInputRef}
          containerStyles={{marginHorizontal: 64, marginBottom: 9}}
          placeholder="Email"
          autoCorrect={false}
          autoCompleteType="email"
          autoCapitalize="none"
          textContentType="emailAddress"
          keyboardType="email-address"
          returnKeyType="next"
          onSubmitEditing={focusPassword}
        />

        <Input
          ref={passwordInputRef}
          containerStyles={{marginHorizontal: 64, marginBottom: 9}}
          placeholder="Password"
          autoCorrect={false}
          textContentType="password"
          autoCapitalize="none"
          autoCompleteType="password"
          returnKeyType="next"
          onSubmitEditing={focusRePassword}
        />

        <Input
          ref={rePasswordInputRef}
          containerStyles={{marginHorizontal: 64, marginBottom: 33}}
          placeholder="Re-enter Password"
          autoCorrect={false}
          textContentType="password"
          autoCapitalize="none"
          autoCompleteType="password"
          returnKeyType="go"
        />

        <Button.NormalButton
          text="Get Started"
          containerStyle={{marginHorizontal: 64, marginBottom: 14}}
          onPress={navigateToConfirmEmail}
        />

        <TouchableOpacity onPress={navigateToLogin}>
          <TextTypes.RegularText
            style={{
              color: colors.black2,
              fontSize: 12,
              fontFamily: 'Nunito-Regular',
              textAlign: 'center',
            }}>
            Already have an account? Log In.
          </TextTypes.RegularText>
        </TouchableOpacity>
      </Col>
    </AuthView>
  );
};

export default SignUpScreen;
