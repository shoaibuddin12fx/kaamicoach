import React from 'react';
import {View} from 'react-native';
import colors from '../../../constants/color';

const Bullet = ({backgroundColor = colors.lightGrey}) => {
  return (
    <View style={{width: 11, height: 11, borderRadius: 5.5, backgroundColor}} />
  );
};

export default Bullet;
