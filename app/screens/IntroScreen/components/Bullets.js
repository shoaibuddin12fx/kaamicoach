import React, {useCallback} from 'react';
import {SafeAreaView} from 'react-native';
import Button from '../../../components/Button';
import colors from '../../../constants/color';
import {Col} from '../../../components/core/Layout';
import Bullet from './Bullet';

const Bullets = ({
  array,
  i,
  navigateFromIntroToLogin,
  navigateFromIntroToSignUp,
}) => {
  const navigateToSignUp = useCallback(() => {
    navigateFromIntroToSignUp();
  }, [navigateFromIntroToSignUp]);

  const navigateToLogin = useCallback(() => {
    navigateFromIntroToLogin();
  }, [navigateFromIntroToLogin]);

  if (array) {
    return (
      <SafeAreaView>
        <Col style={{flexDirection: 'row', justifyContent: 'center'}}>
          {array.map((item, index) => {
            return (
              <Col key={index} style={{marginRight: index == 3 ? 0 : 14}}>
                <Bullet
                  backgroundColor={
                    i == index ? colors.seagreen : colors.lightGrey
                  }
                />
              </Col>
            );
          })}
        </Col>

        <Button.NormalButton
          text="Get Started"
          containerStyle={{
            marginTop: 53,
            marginBottom: 8,
            marginHorizontal: 64,
          }}
          onPress={navigateToSignUp}
        />

        <Button.NormalButton
          mode="ghost"
          text="Log In"
          containerStyle={{marginHorizontal: 64}}
          onPress={navigateToLogin}
        />
      </SafeAreaView>
    );
  } else {
    return null;
  }
};

export default Bullets;
