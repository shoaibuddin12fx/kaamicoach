import React, {useCallback, useState} from 'react';
import {Dimensions, Platform, ScrollView} from 'react-native';
import WelcomeSvg from '../../assets/svg/intro/welcome.svg';
import AskKamiSvg from '../../assets/svg/intro/askkami.svg';
import PersonalSvg from '../../assets/svg/intro/personal.svg';
import ConsultationSvg from '../../assets/svg/intro/consultation.svg';
import colors from '../../constants/color';
import {Col} from '../../components/core/Layout';
import TextTypes from '../../components/Text';
import Bullets from './components/Bullets';
import {useDispatch} from 'react-redux';
import types from '../../redux/types';

const {width, height} = Dimensions.get('window');

const Intro = () => {
  const dispatch = useDispatch();

  const [index, setIndex] = useState(0);

  const onMomentumScrollEnd = useCallback(
    ({nativeEvent}) => {
      const {width} = nativeEvent.layoutMeasurement;

      const activeIndex = Math.floor(
        (nativeEvent.contentOffset.x + width / 2) / width,
      );

      setIndex(activeIndex);
    },
    [setIndex],
  );

  const navigateFromIntroToLogin = useCallback(() => {
    dispatch({type: types.NAVIGATE_FROM_INTRO, initialAuthStackRoute: 'Login'});
  }, [dispatch]);

  const navigateFromIntroToSignUp = useCallback(() => {
    dispatch({
      type: types.NAVIGATE_FROM_INTRO,
      initialAuthStackRoute: 'SignUp',
    });
  }, [dispatch]);

  return (
    <Col style={{flex: 1, backgroundColor: colors.white}}>
      <ScrollView
        horizontal
        scrollEventThrottle={200}
        decelerationRate="fast"
        pagingEnabled
        bounces={false}
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={{backgroundColor: colors.white}}
        onMomentumScrollEnd={onMomentumScrollEnd}>
        <Col style={{width}}>
          <Col
            style={{
              flex: 1,
              backgroundColor: colors.lightBlue,
              alignItems: 'center',
              justifyContent: 'flex-end',
            }}>
            <WelcomeSvg style={{marginBottom: -20}} />
          </Col>
          <Col style={{flex: 1, alignItems: 'center', marginTop: 50}}>
            <TextTypes.SemiBoldText
              style={{fontSize: 22, color: colors.pureBlack}}>
              Welcome to Kami!
            </TextTypes.SemiBoldText>

            <TextTypes.RegularText style={{color: colors.black2}}>
              Made for parents
            </TextTypes.RegularText>
          </Col>
        </Col>

        <Col style={{width}}>
          <Col
            style={{
              flex: 1,
              backgroundColor: colors.lightBlue,
              alignItems: 'center',
              justifyContent: 'flex-end',
            }}>
            <AskKamiSvg style={{marginBottom: -16, marginLeft: 60}} />
          </Col>
          <Col style={{flex: 1, alignItems: 'center', marginTop: 50}}>
            <TextTypes.SemiBoldText
              style={{fontSize: 22, color: colors.pureBlack}}>
              Ask Kami
            </TextTypes.SemiBoldText>

            <TextTypes.RegularText style={{color: colors.black2}}>
              AI Bot created to answer questions 24/7
            </TextTypes.RegularText>
          </Col>
        </Col>

        <Col style={{width}}>
          <Col
            style={{
              flex: 1,
              backgroundColor: colors.lightBlue,
              alignItems: 'center',
              justifyContent: 'flex-end',
            }}>
            <PersonalSvg style={{marginBottom: -16}} />
          </Col>
          <Col style={{flex: 1, alignItems: 'center', marginTop: 50}}>
            <TextTypes.SemiBoldText
              style={{fontSize: 22, color: colors.pureBlack}}>
              Personalised Resources
            </TextTypes.SemiBoldText>

            <TextTypes.RegularText style={{color: colors.black2}}>
              Personalised articles made for you
            </TextTypes.RegularText>
          </Col>
        </Col>

        <Col style={{width}}>
          <Col
            style={{
              flex: 1,
              backgroundColor: colors.lightBlue,
              alignItems: 'center',
              justifyContent: 'flex-end',
            }}>
            <ConsultationSvg style={{marginBottom: -26}} />
          </Col>
          <Col style={{flex: 1, alignItems: 'center', marginTop: 50}}>
            <TextTypes.SemiBoldText
              style={{fontSize: 22, color: colors.pureBlack}}>
              Face to Face Consultations
            </TextTypes.SemiBoldText>

            <TextTypes.RegularText style={{color: colors.black2}}>
              Talk directly with trained consultants
            </TextTypes.RegularText>
          </Col>
        </Col>
      </ScrollView>

      <Col
        style={{
          position: 'absolute',
          left: 0,
          right: 0,
          bottom:
            Platform.OS == 'ios' && height >= 812
              ? 160
              : Platform.OS == 'ios'
              ? 70
              : 100,
        }}>
        <Bullets
          array={new Array(4).fill(0)}
          i={index}
          navigateFromIntroToLogin={navigateFromIntroToLogin}
          navigateFromIntroToSignUp={navigateFromIntroToSignUp}
        />
      </Col>
    </Col>
  );
};

export default Intro;
