import React, {useCallback, useState} from 'react';
import {SafeAreaView} from 'react-native';
import LogoSvg from '../../assets/svg/logo.svg';
import colors from '../../constants/color';
import {Col} from '../../components/core/Layout';
import Content from '../../components/Content';

const InitialChatScreen = () => {
  const [data, setData] = useState([
    {
      content:
        'Hi there! I hope that you are well! My name is Erika. Are you interested in virtual parent coaching?',
      isBot: true,
    },
    {content: 'Yes Please!', isBot: false, userInitial: 'E'},
  ]);

  const responses = [{option: 'Hello'}, {option: 'Hello'}];

  const sendMessage = useCallback(() => {
    let _data = [...data];
    _data.push({
      content:
        'Hi there! I hope that you are well! My name is Erika. Are you interested in virtual parent coaching?',
      isBot: true,
    });

    setData(_data);
  }, [setData]);

  return (
    <SafeAreaView style={{flexGrow: 1, backgroundColor: colors.white}}>
      <Col style={{alignItems: 'center', paddingBottom: 14}}>
        <LogoSvg width={44} height={53} />
      </Col>

      <Content.MessageList data={data} responses={[]} onSend={sendMessage} />
    </SafeAreaView>
  );
};

export default InitialChatScreen;
