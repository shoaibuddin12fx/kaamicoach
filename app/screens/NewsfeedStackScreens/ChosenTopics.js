import BottomTabView from '../../components/BottomTabView';
import React from 'react';
import {FlatList} from 'react-native';
import Content from '../../components/Content';

const ChosenTopics = ({navigation}) => {
  const chosenTopics = [
    {
      id: 0,
      title: 'Mental Health',
      backgroundColor: '#15736B',
    },
    {
      id: 1,
      title: 'Support',
      backgroundColor: '#F08080',
    },
    {
      id: 3,
      title: 'Teething',
      backgroundColor: '#B2DDF7',
    },
    {
      id: 4,
      title: 'Parenting',
      backgroundColor: '#C5C6C6',
    },
    {
      id: 5,
      title: 'Breastfeeding',
      backgroundColor: '#D1BCE3',
    },
    {
      id: 6,
      title: 'Speech Therapy',
      backgroundColor: '#FFB347',
    },
  ];

  const discoverMore = [
    {
      id: 0,
      title: 'Mental Health',
      backgroundColor: '#15736B',
    },
    {
      id: 1,
      title: 'Support',
      backgroundColor: '#F08080',
    },
    {
      id: 3,
      title: 'Teething',
      backgroundColor: '#B2DDF7',
    },
    {
      id: 4,
      title: 'Parenting',
      backgroundColor: '#C5C6C6',
    },
    {
      id: 5,
      title: 'Breastfeeding',
      backgroundColor: '#D1BCE3',
    },
    {
      id: 6,
      title: 'Speech Therapy',
      backgroundColor: '#FFB347',
    },
    {
      id: 7,
      title: 'Mental Health',
      backgroundColor: '#15736B',
    },
    {
      id: 8,
      title: 'Support',
      backgroundColor: '#F08080',
    },
    {
      id: 9,
      title: 'Teething',
      backgroundColor: '#B2DDF7',
    },
    {
      id: 10,
      title: 'Parenting',
      backgroundColor: '#C5C6C6',
    },
    {
      id: 11,
      title: 'Breastfeeding',
      backgroundColor: '#D1BCE3',
    },
    {
      id: 12,
      title: 'Speech Therapy',
      backgroundColor: '#FFB347',
    },
  ];

  return (
    <BottomTabView
      navigation={navigation}
      headerText="Newsfeed"
      useScroll={false}>
      <FlatList
        data={chosenTopics}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{flexGrow: 1, marginBottom: 11}}
        keyExtractor={(item) => item.id.toString()}
        bounces={false}
        numColumns={3}
        ListHeaderComponent={
          <Content.SectionHeading
            text="Chosen Topics"
            containerStyles={{marginBottom: 11}}
          />
        }
        renderItem={({item, index}) => {
          return (
            <Content.Topic
              showCross={true}
              text={item.title}
              backgroundColor={item.backgroundColor}
              containerStyles={{
                flex: 1,
                marginRight: (index + 1) % 3 == 0 ? 0 : 4,
                marginBottom: 8,
              }}
            />
          );
        }}
      />

      <FlatList
        data={discoverMore}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{flexGrow: 1, marginBottom: 11}}
        keyExtractor={(item) => item.id.toString()}
        bounces={false}
        numColumns={3}
        ListHeaderComponent={
          <Content.SectionHeading
            text="Discover More..."
            containerStyles={{marginBottom: 11}}
          />
        }
        renderItem={({item, index}) => {
          return (
            <Content.Topic
              text={item.title}
              backgroundColor={item.backgroundColor}
              containerStyles={{
                flex: 1,
                marginRight: (index + 1) % 3 == 0 ? 0 : 4,
                marginBottom: 8,
              }}
            />
          );
        }}
      />
    </BottomTabView>
  );
};

export default ChosenTopics;
