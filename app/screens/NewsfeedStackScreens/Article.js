import React from 'react';
import BottomTabView from '../../components/BottomTabView';

const Article = ({navigation}) => {
  return (
    <BottomTabView
      navigation={navigation}
      headerText="Newsfeed"></BottomTabView>
  );
};

export default Article;
