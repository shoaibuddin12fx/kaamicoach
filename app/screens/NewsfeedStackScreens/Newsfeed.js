import React, {useCallback, useEffect} from 'react';
import {FlatList, SafeAreaView, TextInput} from 'react-native';
import colors from '../../constants/color';
import LogoSvg from '../../assets/svg/logo.svg';
import {Col, Row} from '../../components/core/Layout';
import SearchSvg from '../../assets/svg/search.svg';
import Content from '../../components/Content';
import BookmarkSvg from '../../assets/svg/components/header/BookmarkSvg';

const NewsfeedScreen = ({navigation, route}) => {
  useEffect(() => {
    if (route.params && route.params.screen) {
      navigation.navigate(route.params.screen);
    }
  });

  const chosenTopics = [
    {
      id: 0,
      title: 'Mental Health',
      backgroundColor: '#15736B',
    },
    {
      id: 1,
      title: 'Support',
      backgroundColor: '#F08080',
    },
    {
      id: 3,
      title: 'Teething',
      backgroundColor: '#B2DDF7',
    },
    {
      id: 4,
      title: 'Parenting',
      backgroundColor: '#C5C6C6',
    },
    {
      id: 5,
      title: 'Breastfeeding',
      backgroundColor: '#D1BCE3',
    },
    {
      id: 6,
      title: 'Speech Therapy',
      backgroundColor: '#FFB347',
    },
  ];

  const newsfeed = [
    {
      image: 'https://picsum.photos/seed/picsum/300/200',
      author: 'Patrick A. Coleman',
      status: 'Parent',
      title: 'How to Turn Happry Childresn Into Happy Adults',
      description:
        'Being the parent of a kid who "has it all" and yet no happiness to show for it is all too common....',
    },
    {
      image: 'https://picsum.photos/seed/picsum/300/200',
      author: 'Rachel Smith',
      status: 'Midwife at Barkantine Birth Centre',
      title: 'Why Peer Support is Vital',
    },
    {
      image: 'https://picsum.photos/seed/picsum/300/200',
      author: 'Rachel Smith',
      status: 'Midwife at Barkantine Birth Centre',
      title: 'Why Peer Support is Vital',
    },
  ];

  const navigateToChosenTopics = useCallback(() => {
    navigation.navigate('ChosenTopics');
  }, [navigation]);

  const navigateToSavedArticles = useCallback(() => {
    navigation.navigate('SavedArticles');
  }, [navigation]);

  const navigateToArticle = useCallback(() => {
    navigation.navigate('Article');
  }, [navigation]);

  return (
    <SafeAreaView style={{flex: 1, flexGrow: 1, backgroundColor: colors.white}}>
      <Col style={{flex: 1, paddingHorizontal: 25}}>
        <Row style={{marginBottom: 37}}>
          <Col style={{flex: 1}}>
            <LogoSvg width={41} height={49} />
          </Col>
          <BookmarkSvg onPress={navigateToSavedArticles} />
        </Row>

        <Row
          style={{
            backgroundColor: colors.grey,
            borderRadius: 10,
            padding: 12,
            marginBottom: 20,
          }}>
          <SearchSvg />
          <TextInput
            placeholder="Search Articles"
            placeholderTextColor={colors.lightGrey}
            style={{
              flex: 1,
              textAlignVertical: 'center',
              marginLeft: 8,
              fontFamily: 'Nunito-Regular',
              fontSize: 18,
              fontWeight: '400',
              color: colors.pureBlack,
            }}
          />
        </Row>

        <FlatList
          data={newsfeed}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item, index) => index.toString()}
          ListHeaderComponent={
            <>
              <Content.SectionHeading
                text="Chosen Topics"
                showButton={true}
                containerStyles={{marginBottom: 12}}
                onPress={navigateToChosenTopics}
              />

              <Col style={{marginBottom: 26}}>
                <FlatList
                  data={chosenTopics}
                  keyExtractor={(item) => item.id.toString()}
                  numColumns={3}
                  renderItem={({item, index}) => {
                    return (
                      <Content.Topic
                        text={item.title}
                        backgroundColor={item.backgroundColor}
                        containerStyles={{
                          flex: 1,
                          marginRight: (index + 1) % 3 == 0 ? 0 : 4,
                          marginBottom: 8,
                        }}
                      />
                    );
                  }}
                />
              </Col>

              <Content.SectionHeading
                text="Newsfeed"
                containerStyles={{marginBottom: 8}}
              />
            </>
          }
          renderItem={({item}) => {
            return (
              <Content.ArticleCard
                author={item.author}
                status={item.status}
                title={item.title}
                description={item.description}
                image={item.image}
                containerStyles={{marginBottom: 36}}
                onPress={navigateToArticle}
              />
            );
          }}
        />
      </Col>
    </SafeAreaView>
  );
};

export default NewsfeedScreen;
