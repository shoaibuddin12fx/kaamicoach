import React from 'react';
import {FlatList} from 'react-native';
import BottomTabView from '../../components/BottomTabView';
import Content from '../../components/Content';

const SavedArticles = ({navigation}) => {
  const newsfeed = [
    {
      image: 'https://picsum.photos/seed/picsum/300/200',
      author: 'Patrick A. Coleman',
      status: 'Parent',
      title: 'How to Turn Happry Childresn Into Happy Adults',
      description:
        'Being the parent of a kid who "has it all" and yet no happiness to show for it is all too common....',
    },
    {
      image: 'https://picsum.photos/seed/picsum/300/200',
      author: 'Rachel Smith',
      status: 'Midwife at Barkantine Birth Centre',
      title: 'Why Peer Support is Vital',
    },
    {
      image: 'https://picsum.photos/seed/picsum/300/200',
      author: 'Rachel Smith',
      status: 'Midwife at Barkantine Birth Centre',
      title: 'Why Peer Support is Vital',
    },
  ];

  return (
    <BottomTabView
      navigation={navigation}
      headerText="Newsfeed"
      useScroll={false}
      containerStyles={{flex: 1}}
      svgContainerStyles={{marginBottom: 14}}>
      <FlatList
        data={newsfeed}
        showsVerticalScrollIndicator={false}
        keyExtractor={(item, index) => index.toString()}
        ListHeaderComponent={
          <Content.SectionHeading
            text="Saved Articles"
            containerStyles={{marginBottom: 17}}
          />
        }
        renderItem={({item}) => {
          return (
            <Content.ArticleCard
              showBookmark={true}
              author={item.author}
              status={item.status}
              title={item.title}
              description={item.description}
              image={item.image}
              containerStyles={{marginBottom: 36}}
            />
          );
        }}
      />
    </BottomTabView>
  );
};

export default SavedArticles;
