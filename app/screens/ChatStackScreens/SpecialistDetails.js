import React from 'react';
import {Image} from 'react-native';
import BottomTabView from '../../components/BottomTabView';
import Button from '../../components/Button';
import colors from '../../constants/color';
import Content from '../../components/Content';
import TextTypes from '../../components/Text';
import ExpertiseArea from './components/ExpertiseArea';
import Heading from './components/Heading';

const SpecialistDetails = ({navigation}) => {
  return (
    <BottomTabView
      navigation={navigation}
      headerText="Specialists"
      svgContainerStyles={{marginBottom: 25}}>
      <Content.SectionHeading text="Tamara Lichd" />

      <TextTypes.RegularText
        style={{
          color: colors.black,
          fontSize: 12,
          fontFamily: 'Nunito-Regular',
          fontWeight: '300',
          marginBottom: 11,
        }}>
        Resident Psychologist // Relationships and Managing Expectations
      </TextTypes.RegularText>

      <Image
        source={{uri: 'https://i.pravatar.cc/300'}}
        style={{width: 101, height: 101, borderRadius: 50.5, marginBottom: 15}}
        resizeMode="cover"
      />

      <Heading text="Areas of Expertise" containerStyles={{marginBottom: 19}} />

      <ExpertiseArea
        expertise="Cognitive Behavioral Therapy"
        description="To treat depression, anxiety, PTSD, Social phobia"
        containerStyles={{marginRight: 30, marginBottom: 8}}
      />

      <ExpertiseArea
        expertise="Relationship Difficulties"
        description="Using techniques to help work through relationship"
        containerStyles={{marginRight: 30, marginBottom: 8}}
      />

      <ExpertiseArea
        expertise="Managing Expectations"
        description="How to reach the right expectations for your pregnancy and early parenting journey"
        containerStyles={{marginRight: 30, marginBottom: 8}}
      />

      <ExpertiseArea
        expertise="Building Self-esteem"
        description="Ensuring that you feel self-assured in your journey"
        containerStyles={{marginRight: 30, marginBottom: 10}}
      />

      <Heading text="Qualifications" containerStyles={{marginBottom: 19}} />

      <TextTypes.RegularText
        style={{
          color: colors.pureBlack,
          fontSize: 14,
          fontFamily: 'Nunito-Regular',
          fontWeight: '400',
          marginBottom: 20,
          textAlign: 'justify',
        }}>
        HCPC accredited Clinical Psychologist and counselling psychologist.
        Master in Clinical and Health Psychology. Master of Social Sciences.
        Bachelor in Clinical Psychology.
      </TextTypes.RegularText>

      <Heading text="Experience" containerStyles={{marginBottom: 19}} />

      <TextTypes.RegularText
        style={{
          color: colors.pureBlack,
          fontSize: 14,
          fontFamily: 'Nunito-Regular',
          fontWeight: '400',
          marginBottom: 20,
          textAlign: 'justify',
        }}>
        I work with new and expectant parents and couples offering psychological
        assessments and therapeutic interventions mostly under CBT and
        pshycodynamic principles.
      </TextTypes.RegularText>

      <Heading text="How Can I Help?" containerStyles={{marginBottom: 19}} />

      <TextTypes.RegularText
        style={{
          color: colors.pureBlack,
          fontSize: 14,
          fontFamily: 'Nunito-Regular',
          fontWeight: '400',
          marginBottom: 20,
          textAlign: 'justify',
        }}>
        If you are struggling to deal with your emotions and feelings i can help
        support you and tackle these feelings during your pregnancy journey.
      </TextTypes.RegularText>

      <Button.NormalButton
        text="Schedule Video Call"
        containerStyle={{marginHorizontal: 40, marginBottom: 8}}
      />

      <Button.NormalButton
        mode="ghost"
        text="Schedule Chat"
        containerStyle={{marginHorizontal: 40, marginBottom: 36}}
      />
    </BottomTabView>
  );
};

export default SpecialistDetails;
