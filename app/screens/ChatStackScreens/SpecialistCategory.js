import React, {useCallback} from 'react';
import {FlatList} from 'react-native';
import BottomTabView from '../../components/BottomTabView';
import Content from '../../components/Content';
import SpecialistCategoryCard from './components/SpecialistCategoryCard';

const SpecialistCategory = ({navigation}) => {
  const specialistCategoryList = [
    {
      title: 'Child Development',
      image: 'https://picsum.photos/seed/picsum/300/200',
      description:
        'Pregnancy and the physical, language, thought and emotional changes that occur in a child from birth to the beginning of adulthood.',
    },
    {
      title: 'Psychologist',
      image: 'https://picsum.photos/seed/picsum/300/200',
      description:
        'The branch of medicine devoted to the diagnosis, prevention, study, and treatment of mental disorders.',
    },
    {
      title: 'Parental Wellness',
      image: 'https://picsum.photos/seed/picsum/300/200',
      description:
        'Wellness is an active process of becoming aware of and making choices toward a healthy and fulfilling life. Wellness is a dynamic process of change and growth.',
    },
    {
      title: 'Physical Therapist',
      image: 'https://picsum.photos/seed/picsum/300/200',
      description:
        'Licensed professional who helps clients improve their lives, develop better cognitive and emotional skills, reduce symptoms of mental and physical illness and cope with various challenges.',
    },
  ];

  const navigateToSpecialistList = useCallback(() => {
    navigation.navigate('SpecialistList');
  }, [navigation]);

  return (
    <BottomTabView
      navigation={navigation}
      headerText="Kami"
      useScroll={false}
      containerStyles={{flex: 1}}
      svgContainerStyles={{marginBottom: 25}}>
      <FlatList
        data={specialistCategoryList}
        contentContainerStyle={{flexGrow: 1, marginHorizontal: 10}}
        keyExtractor={(item, index) => index.toString()}
        showsVerticalScrollIndicator={false}
        numColumns={2}
        renderItem={({item, index}) => {
          return (
            <SpecialistCategoryCard
              image={item.image}
              title={item.title}
              description={item.description}
              onPress={navigateToSpecialistList}
              containerStyles={{
                marginRight: (index + 1) % 2 == 0 ? 0 : 28,
                marginBottom: 20,
              }}
            />
          );
        }}
        ListHeaderComponent={
          <Content.SectionHeading
            text="Schedule a Video Call or Chat with Expert"
            containerStyles={{marginBottom: 20}}
          />
        }
      />
    </BottomTabView>
  );
};

export default SpecialistCategory;
