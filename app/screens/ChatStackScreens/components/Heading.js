import React from 'react';
import colors from '../../../constants/color';
import {Col} from '../../../components/core/Layout';
import TextTypes from '../../../components/Text';

const Heading = ({text, containerStyles}) => {
  return (
    <Col style={[{flexWrap: 'wrap'}, containerStyles]}>
      <TextTypes.RegularText
        style={{
          fontFamily: 'Nunito-Regular',
          fontSize: 14,
          fontWeight: '400',
          color: colors.seagreen,
          borderWidth: 1,
          paddingVertical: 8,
          paddingHorizontal: 18,
          borderRadius: 10,
          borderColor: colors.seagreen,
          backgroundColor: colors.grey,
        }}>
        {text}
      </TextTypes.RegularText>
    </Col>
  );
};

export default Heading;
