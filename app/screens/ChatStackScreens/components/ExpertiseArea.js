import React from 'react';
import colors from '../../../constants/color';
import {Col} from '../../../components/core/Layout';
import TextTypes from '../../../components/Text';

const ExpertiseArea = ({expertise, description, containerStyles}) => {
  return (
    <Col
      style={[
        {
          backgroundColor: colors.grey,
          paddingTop: 11,
          paddingLeft: 9,
          paddingRight: 24,
          borderRadius: 10,
        },
        containerStyles,
      ]}>
      <TextTypes.RegularText
        style={{
          fontFamily: 'Nunito-Regular',
          fontSize: 14,
          fontWeight: '400',
          color: colors.pureBlack,
          marginBottom: 6,
        }}>
        {expertise}
      </TextTypes.RegularText>

      <TextTypes.RegularText
        style={{
          fontFamily: 'Nunito-Regular',
          fontSize: 12,
          fontWeight: '300',
          color: colors.pureBlack,
          marginBottom: 16,
        }}>
        {description}
      </TextTypes.RegularText>
    </Col>
  );
};

export default ExpertiseArea;
