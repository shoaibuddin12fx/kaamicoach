import React from 'react';
import {Image, TouchableOpacity} from 'react-native';
import RightSvg from '../../../assets/svg/components/RightSvg';
import colors from '../../../constants/color';
import {Col, Row} from '../../../components/core/Layout';
import TextTypes from '../../../components/Text';

const SpecialistCategoryCard = ({
  image,
  title,
  description,
  containerStyles,
  onPress,
}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[{flex: 1, backgroundColor: colors.grey}, containerStyles]}>
      <Image source={{uri: image}} style={{height: 102}} resizeMode="cover" />

      <Col style={{marginTop: 14, marginHorizontal: 10, marginBottom: 11}}>
        <TextTypes.RegularText
          style={{
            fontFamily: 'Nunito-Regular',
            fontSize: 14,
            fontWeight: '400',
            color: colors.pureBlack,
          }}>
          {title}
        </TextTypes.RegularText>

        <TextTypes.RegularText
          style={{
            fontFamily: 'Nunito-Regular',
            fontSize: 12,
            fontWeight: '300',
            color: colors.pureBlack,
            height: 126,
          }}>
          {description}
        </TextTypes.RegularText>
      </Col>

      <Row
        style={{
          borderTopWidth: 1,
          borderTopColor: colors.lightGrey3,
          paddingTop: 13,
          paddingLeft: 9,
          paddingBottom: 2,
        }}>
        <TextTypes.RegularText
          style={{
            fontFamily: 'Nunito-Regular',
            fontSize: 14,
            fontWeight: '400',
            color: colors.pureBlack,
          }}>
          Find Specialist
        </TextTypes.RegularText>

        <RightSvg style={{marginLeft: 5}} />
      </Row>
    </TouchableOpacity>
  );
};

export default SpecialistCategoryCard;
