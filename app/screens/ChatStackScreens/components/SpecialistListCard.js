import React from 'react';
import {Image, TouchableOpacity} from 'react-native';
import RightSvg from '../../../assets/svg/components/RightSvg';
import colors from '../../../constants/color';
import {Col, Row} from '../../../components/core/Layout';
import TextTypes from '../../../components/Text';

const SpecialistListCard = ({
  image,
  name,
  profession,
  onReadMorePress,
  onScheduleNowPress,
}) => {
  return (
    <Col style={{marginHorizontal: 41}}>
      <Image
        style={{
          width: 87,
          height: 87,
          borderRadius: 43.5,
          alignSelf: 'center',
          overflow: 'hidden',
          marginBottom: 10,
        }}
        resizeMode="cover"
        source={{uri: image}}
      />

      <Col
        style={{
          backgroundColor: colors.grey,
          borderRadius: 10,
          paddingTop: 9,
          marginBottom: 37,
        }}>
        <TextTypes.RegularText
          style={{
            color: colors.black,
            textAlign: 'center',
            fontWeight: '400',
          }}>
          {name}
        </TextTypes.RegularText>

        <TextTypes.RegularText
          style={{
            color: colors.black,
            textAlign: 'center',
            fontWeight: '300',
            fontSize: 12,
            fontFamily: 'Nunito-Regular',
            marginBottom: 7,
          }}>
          {profession}
        </TextTypes.RegularText>

        <Row
          style={{
            borderTopWidth: 1,
            borderTopColor: colors.lightGrey3,
          }}>
          <TouchableOpacity
            onPress={onReadMorePress}
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              flex: 1,
              borderRightWidth: 1,
              borderRightColor: colors.lightGrey3,
              marginLeft: 15,
              paddingTop: 11,
            }}>
            <TextTypes.RegularText
              style={{
                color: colors.pureBlack,
                fontWeight: '400',
                fontSize: 14,
                fontFamily: 'Nunito-Regular',
              }}>
              Read More
            </TextTypes.RegularText>

            <RightSvg style={{marginLeft: 5}} />
          </TouchableOpacity>

          <TouchableOpacity
            onPress={onScheduleNowPress}
            style={{
              flex: 1,
              marginLeft: 15,
              paddingTop: 11,
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <TextTypes.RegularText
              style={{
                color: colors.pureBlack,
                fontWeight: '400',
                fontSize: 14,
                fontFamily: 'Nunito-Regular',
              }}>
              Schedule Now
            </TextTypes.RegularText>

            <RightSvg style={{marginLeft: 5}} />
          </TouchableOpacity>
        </Row>
      </Col>
    </Col>
  );
};

export default SpecialistListCard;
