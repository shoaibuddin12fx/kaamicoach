import React from 'react';
import {SafeAreaView, Text, View} from 'react-native';
import {Col} from '../../components/core/Layout';
import LogoSvg from '../../assets/svg/logo.svg';
import Content from '../../components/Content';
import colors from '../../constants/color';

const ChatScreen = () => {
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: colors.white}}>
      <Col style={{alignItems: 'center'}}>
        <LogoSvg width={44} height={53} />
      </Col>
      <Content.MessageList />
    </SafeAreaView>
  );
};

export default ChatScreen;
