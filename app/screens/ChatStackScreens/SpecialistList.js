import React, {useCallback} from 'react';
import {FlatList} from 'react-native';
import BottomTabView from '../../components/BottomTabView';
import Content from '../../components/Content';
import SpecialistListCard from './components/SpecialistListCard';

const SpecialistList = ({navigation}) => {
  const specialistList = [
    {
      image: 'https://i.pravatar.cc/300',
      name: 'Tamara Lichd',
      profession: 'Resident Psychologist',
    },
    {
      image: 'https://i.pravatar.cc/300',
      name: 'Tamara Lichd',
      profession: 'Resident Psychologist',
    },
    {
      image: 'https://i.pravatar.cc/300',
      name: 'Tamara Lichd',
      profession: 'Resident Psychologist',
    },
    {
      image: 'https://i.pravatar.cc/300',
      name: 'Tamara Lichd',
      profession: 'Resident Psychologist',
    },
    {
      image: 'https://i.pravatar.cc/300',
      name: 'Tamara Lichd',
      profession: 'Resident Psychologist',
    },
    {
      image: 'https://i.pravatar.cc/300',
      name: 'Tamara Lichd',
      profession: 'Resident Psychologist',
    },
  ];

  const navigateToSpecialistDetails = useCallback(() => {
    navigation.navigate('SpecialistDetails');
  }, [navigation]);

  return (
    <BottomTabView
      navigation={navigation}
      useScroll={false}
      headerText="Kami"
      containerStyles={{flex: 1}}
      svgContainerStyles={{marginBottom: 25}}>
      <FlatList
        data={specialistList}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{flexGrow: 1, marginHorizontal: 10}}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item}) => {
          return (
            <SpecialistListCard
              image={item.image}
              name={item.name}
              profession={item.profession}
              onReadMorePress={navigateToSpecialistDetails}
            />
          );
        }}
        ListHeaderComponent={
          <Content.SectionHeading
            text="Psychologists"
            containerStyles={{marginBottom: 28}}
          />
        }
      />
    </BottomTabView>
  );
};

export default SpecialistList;
