const local = {
  api: 'http://33cfd9bf5162.ngrok.io',
};

const dev = {
  api: 'http://kamicooach-alb-708501411.eu-west-2.elb.amazonaws.com:1337',
};

const live = {
  api: 'http://kamicooach-alb-708501411.eu-west-2.elb.amazonaws.com:1337',
};

export {local, dev, live};
