const colors = {
  white: '#FFFFFF',
  seagreen: '#37C0B0',
  lightBlue: '#E5F1F0',
  grey: '#F4F4F4',
  lightGrey: '#C5C6C6',
  lightGrey2: '#C4C4C4',
  lightGrey3: '#C2C2C2',
  lightGrey4: '#969998',
  darkGrey: '#CBCCCC',
  darkGrey2: '#686B69',
  darkGreen: '#15736B',
  black: '#0A100D',
  black2: '#393E3B',
  pureBlack: '#000000',
  red: '#FF0000',
};

export default colors;
