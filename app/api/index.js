import axios from 'axios';
import {local, dev, live} from '../constants/environment';

const Api = axios.create({
  baseURL: local.api,
  timeout: 10000,
});

export default Api;
