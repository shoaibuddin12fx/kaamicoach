import React from 'react';
import Svg, {Circle, Path} from 'react-native-svg';

function AddSvg({color = '#37C0B0', width = 30, height = 31}) {
  return (
    <Svg
      width={width}
      height={height}
      viewBox="0 0 30 31"
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <Circle cx={15} cy={15.5} r={13} stroke={color} />
      <Path
        d="M15.167 11v9.333M10.5 15.667h9.333"
        stroke={color}
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  );
}

export default AddSvg;
