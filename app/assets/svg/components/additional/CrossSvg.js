import React from 'react';
import Svg, {G, Circle, Path, Defs, ClipPath} from 'react-native-svg';

function CrossSvg({color = '#37C0B0', width = 16, height = 16}) {
  return (
    <Svg
      width={width}
      height={height}
      viewBox="0 0 16 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <G clipPath="url(#prefix__clip0)" stroke={color}>
        <Circle
          cx={7.663}
          cy={8.163}
          r={4.378}
          transform="rotate(-46.097 7.663 8.163)"
          fill="#fff"
        />
        <Path
          d="M6.533 6.992l2.43 2.339m-2.384.045l2.339-2.43"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </G>
      <Defs>
        <ClipPath id="prefix__clip0">
          <Path
            fill="#fff"
            transform="rotate(-46.097 9.765 4.155)"
            d="M0 0h10.839v10.839H0z"
          />
        </ClipPath>
      </Defs>
    </Svg>
  );
}

export default CrossSvg;
