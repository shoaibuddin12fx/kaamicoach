import React from 'react';
import Svg, {Path} from 'react-native-svg';

function PlusSvg({color = '#F4F4F4', width = 15, height = 15}) {
  return (
    <Svg
      width={width}
      height={height}
      viewBox="0 0 15 15"
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <Path
        d="M7.222 1.5v12.444M1 7.722h12.444"
        stroke={color}
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  );
}

export default PlusSvg;
