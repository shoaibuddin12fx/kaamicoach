import React from 'react';
import Svg, {Path} from 'react-native-svg';

function RightSvg({color = '#393E3B', width = 22, height = 11, style}) {
  return (
    <Svg
      style={style}
      width={width}
      height={height}
      viewBox="0 0 22 11"
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <Path
        d="M1.001 5.5h20m0 0l-4.852-5m4.852 5l-4.852 5"
        stroke={color}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  );
}

export default RightSvg;
