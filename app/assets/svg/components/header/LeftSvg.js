import React from 'react';
import {TouchableOpacity} from 'react-native';
import Svg, {Path} from 'react-native-svg';

function LeftSvg({
  color = '#37C0B0',
  width = 16,
  height = 13,
  style,
  onPress,
  customContent,
  containerStyles,
}) {
  return (
    <TouchableOpacity onPress={onPress} style={containerStyles}>
      <Svg
        style={style}
        width={width}
        height={height}
        viewBox="0 0 16 13"
        fill="none"
        xmlns="http://www.w3.org/2000/svg">
        <Path
          d="M15 6.5H1m0 0l5 5m-5-5l5-5"
          stroke={color}
          strokeWidth={2}
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </Svg>
      {customContent && customContent}
    </TouchableOpacity>
  );
}

export default LeftSvg;
