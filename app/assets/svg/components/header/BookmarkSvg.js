import React from 'react';
import {TouchableOpacity} from 'react-native';
import Svg, {Path} from 'react-native-svg';

function BookmarkSvg({
  color = '#37C0B0',
  fill = 'none',
  width = 16,
  height = 21,
  onPress,
}) {
  return (
    <TouchableOpacity onPress={onPress}>
      <Svg
        width={width}
        height={height}
        viewBox="0 0 16 21"
        fill={fill}
        xmlns="http://www.w3.org/2000/svg">
        <Path
          d="M15 19.5l-7-5-7 5v-16a2 2 0 012-2h10a2 2 0 012 2v16z"
          stroke={color}
          strokeWidth={2}
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </Svg>
    </TouchableOpacity>
  );
}

export default BookmarkSvg;
