import React from 'react';
import Svg, {Path} from 'react-native-svg';

function RightSvg({color = '#37C0B0', width = 16, height = 13}) {
  return (
    <Svg
      width={width}
      height={height}
      viewBox="0 0 16 13"
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <Path
        d="M1 6.5h14m0 0l-5-5m5 5l-5 5"
        stroke={color}
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  );
}

export default RightSvg;
