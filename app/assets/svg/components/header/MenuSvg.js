import React from 'react';
import Svg, {Path} from 'react-native-svg';

function MenuSvg({color = '#37C0B0', width = 20, height = 15}) {
  return (
    <Svg
      width={width}
      height={height}
      viewBox="0 0 20 15"
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <Path
        d="M1 7.5h18m-18-6h18m-18 12h18"
        stroke={color}
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  );
}

export default MenuSvg;
