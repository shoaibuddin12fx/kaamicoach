import React from 'react';
import Svg, {Path} from 'react-native-svg';

function ProfileSvg({color = '#C5C6C6', width = 27, height = 31}) {
  return (
    <Svg
      width={width}
      height={height}
      viewBox="0 0 27 31"
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <Path
        d="M26 29.5v-3.111c0-1.65-.659-3.233-1.83-4.4-1.173-1.167-5.762-1.822-7.42-1.822h-6.5c-1.658 0-6.247.655-7.42 1.822A6.208 6.208 0 001 26.39V29.5M19.75 7.722c0 3.437-2.798 6.222-6.25 6.222S7.25 11.16 7.25 7.722c0-3.436 2.798-6.222 6.25-6.222s6.25 2.786 6.25 6.222z"
        stroke={color}
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  );
}

export default ProfileSvg;
