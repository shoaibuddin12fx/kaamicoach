import React from 'react';
import Svg, {Path} from 'react-native-svg';

function HomeSvg({color = '#C5C6C6', width = 24, height = 29}) {
  return (
    <Svg
      width={width}
      height={height}
      viewBox="0 0 25 29"
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <Path
        d="M8.667 27.5v-13h7.666v13M1 10.6l11.5-9.1L24 10.6v14.3c0 .69-.27 1.35-.748 1.838-.48.488-1.13.762-1.808.762H3.556a2.534 2.534 0 01-1.808-.762A2.623 2.623 0 011 24.9V10.6z"
        stroke={color}
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  );
}

export default HomeSvg;
