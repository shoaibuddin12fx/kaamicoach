import React from 'react';
import Svg, {Path} from 'react-native-svg';

function ChatSvg({color = '#C5C6C6', width = 39, height = 35}) {
  return (
    <Svg
      width={width}
      height={height}
      viewBox="0 0 39 35"
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <Path
        d="M15 25.473c7.732 0 14-5.367 14-11.987S22.732 1.5 15 1.5 1 6.866 1 13.486c0 2.4.823 4.633 2.24 6.507L1.667 26.5l5.666-2.983c2.203 1.237 4.837 1.956 7.667 1.956z"
        stroke={color}
        strokeWidth={2}
        strokeLinejoin="round"
      />
      <Path
        d="M27.5 32.801c-5.799 0-10.5-3.649-10.5-8.15 0-4.502 4.701-8.151 10.5-8.151S38 20.15 38 24.65c0 1.632-.617 3.151-1.68 4.425L37.5 33.5l-4.25-2.028c-1.652.84-3.627 1.33-5.75 1.33z"
        fill={color}
        stroke={color}
        strokeWidth={2}
        strokeLinejoin="round"
      />
    </Svg>
  );
}

export default ChatSvg;
