import React from 'react';
import Svg, {Path} from 'react-native-svg';

function Newsfeed({color = '#C5C6C6', width = 31, height = 28, onPress}) {
  return (
    <Svg
      width={width}
      height={height}
      viewBox="0 0 31 28"
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <Path
        d="M15.5 6.778a5.767 5.767 0 00-1.699-4.086A5.811 5.811 0 009.7 1H1v21.667h10.15c1.154 0 2.26.456 3.076 1.269A4.325 4.325 0 0115.5 27m0-20.222V27m0-20.222c0-1.533.611-3.002 1.699-4.086A5.811 5.811 0 0121.3 1H30v21.667H19.85c-1.154 0-2.26.456-3.076 1.269A4.325 4.325 0 0015.5 27"
        stroke={color}
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  );
}

export default Newsfeed;
