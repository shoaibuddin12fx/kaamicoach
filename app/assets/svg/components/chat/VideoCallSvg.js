import React from 'react';
import Svg, {Path} from 'react-native-svg';

function VideoCallSvg({color = '#37C0B0', width = 22, height = 16}) {
  return (
    <Svg
      width={width}
      height={height}
      viewBox="0 0 22 16"
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <Path
        d="M21 3.318l-6.364 4.546L21 12.409V3.32z"
        stroke={color}
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <Path
        d="M12.818 1.5h-10A1.818 1.818 0 001 3.318v9.091c0 1.004.814 1.818 1.818 1.818h10a1.818 1.818 0 001.818-1.818V3.32A1.818 1.818 0 0012.818 1.5z"
        stroke={color}
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  );
}

export default VideoCallSvg;
