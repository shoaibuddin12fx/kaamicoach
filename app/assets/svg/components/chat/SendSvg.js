import React from 'react';
import Svg, {Circle, Path} from 'react-native-svg';

function SendSvg({color = '#37C0B0', width = 24, height = 25}) {
  return (
    <Svg
      width={width}
      height={height}
      viewBox="0 0 24 25"
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <Circle cx={12} cy={12.5} r={12} fill={color} />
      <Path
        d="M16.076 6.632l-2.46 12.773L10.7 14.31l-5.785-1 11.162-6.679z"
        stroke="#F4F4F4"
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  );
}

export default SendSvg;
