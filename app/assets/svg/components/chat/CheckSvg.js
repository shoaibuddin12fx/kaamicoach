import React from 'react';
import Svg, {Path} from 'react-native-svg';

function CheckSvg({color = '#37C0B0', width = 22, height = 23}) {
  return (
    <Svg
      width={width}
      height={height}
      viewBox="0 0 22 23"
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <Path
        d="M21 10.586v.92a10 10 0 11-5.93-9.14M21 3.506l-10 10.01-3-3"
        stroke={color}
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  );
}

export default CheckSvg;
