import React from 'react';
import Svg, {Path} from 'react-native-svg';

function CheckCircleSvg({color = '#37C0B0', width = 24, height = 25}) {
  return (
    <Svg
      width={width}
      height={height}
      viewBox="0 0 24 25"
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <Path
        d="M12 24.5c6.627 0 12-5.373 12-12S18.627.5 12 .5 0 5.873 0 12.5s5.373 12 12 12z"
        fill={color}
      />
    </Svg>
  );
}

export default CheckCircleSvg;
