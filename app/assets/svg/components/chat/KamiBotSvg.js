import React from 'react';
import Svg, {Circle, Path} from 'react-native-svg';

function KamiBotSvg({color = '#37C0B0', width = 24, height = 25}) {
  return (
    <Svg
      width={width}
      height={height}
      viewBox="0 0 24 25"
      fill="none"
      xmlns="http://www.w3.org/2000/svg">
      <Circle cx={12} cy={12.5} r={12} fill={color} />
      <Path
        d="M16.692 18.384c.168.156.252.33.252.522 0 .168-.072.324-.216.468a.64.64 0 01-.468.198c-.156 0-.318-.072-.486-.216L9.042 13.47v5.364a.794.794 0 01-.198.558c-.132.132-.312.198-.54.198-.228 0-.408-.066-.54-.198a.794.794 0 01-.198-.558V7.476c0-.228.066-.408.198-.54.132-.144.312-.216.54-.216.228 0 .408.072.54.216.132.132.198.312.198.54v5.166l6.426-5.706a.64.64 0 01.468-.198c.18 0 .336.072.468.216a.62.62 0 01.198.45c0 .18-.078.348-.234.504l-5.814 5.112 6.138 5.364z"
        fill="#fff"
      />
    </Svg>
  );
}

export default KamiBotSvg;
