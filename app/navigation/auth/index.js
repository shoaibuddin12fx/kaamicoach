import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import {useSelector} from 'react-redux';
import CreateNewPasswordScreen from '../../screens/AuthScreens/CreateNewPassword';
import EmailVerifiedScreen from '../../screens/AuthScreens/EmailVerified';
import ConfirmEmailScreen from '../../screens/AuthScreens/ConfirmEmail';
import EnterCodeScreen from '../../screens/AuthScreens/EnterCode';
import ForgotPasswordScreen from '../../screens/AuthScreens/ForgotPassword';
import LoginScreen from '../../screens/AuthScreens/Login';
import SignUpScreen from '../../screens/AuthScreens/SignUp';
import LeftSvg from '../../assets/svg/components/header/LeftSvg';

const AuthStack = createStackNavigator();

const Auth = () => {
  const initialAuthStackRoute = useSelector(
    (state) => state.auth.initialAuthStackRoute,
  );

  return (
    <AuthStack.Navigator
      screenOptions={{headerTransparent: true}}
      initialRouteName={initialAuthStackRoute}>
      <AuthStack.Screen
        name="Login"
        options={{headerShown: false}}
        component={LoginScreen}
      />
      <AuthStack.Screen
        name="SignUp"
        options={{headerShown: false}}
        component={SignUpScreen}
      />
      <AuthStack.Screen
        name="ForgotPassword"
        component={ForgotPasswordScreen}
        options={({navigation}) => ({
          headerTitle: '',
          headerLeft: () => (
            <LeftSvg
              onPress={() => navigation.goBack()}
              style={{marginLeft: 18}}
            />
          ),
        })}
      />
      <AuthStack.Screen
        name="CreateNewPassword"
        component={CreateNewPasswordScreen}
        options={{headerTransparent: true, headerTitle: ''}}
      />
      <AuthStack.Screen
        name="EnterCode"
        component={EnterCodeScreen}
        options={({navigation}) => ({
          headerTitle: '',
          headerLeft: () => (
            <LeftSvg
              onPress={() => navigation.goBack()}
              style={{marginLeft: 18}}
            />
          ),
        })}
      />
      <AuthStack.Screen
        name="EmailVerified"
        component={EmailVerifiedScreen}
        options={({navigation}) => ({
          headerTitle: '',
          headerLeft: () => (
            <LeftSvg
              onPress={() => navigation.goBack()}
              style={{marginLeft: 18}}
            />
          ),
        })}
      />

      <AuthStack.Screen
        name="ConfirmEmail"
        component={ConfirmEmailScreen}
        options={({navigation}) => ({
          headerTitle: '',
          headerLeft: () => (
            <LeftSvg
              onPress={() => navigation.goBack()}
              style={{marginLeft: 18}}
            />
          ),
        })}
      />
    </AuthStack.Navigator>
  );
};

export default Auth;
