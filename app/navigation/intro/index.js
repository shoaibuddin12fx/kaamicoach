import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import IntroScreen from '../../screens/IntroScreen';

const IntroStack = createStackNavigator();

const Intro = () => {
  return (
    <IntroStack.Navigator headerMode="none">
      <IntroStack.Screen name="Intro" component={IntroScreen} />
    </IntroStack.Navigator>
  );
};

export default Intro;
