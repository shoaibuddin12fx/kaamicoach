import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import InitialChatScreen from '../../screens/InitialChatScreen';

const InitialChatStack = createStackNavigator();

const InitialChat = () => {
  return (
    <InitialChatStack.Navigator headerMode="none">
      <InitialChatStack.Screen
        name="InitialChat"
        component={InitialChatScreen}
      />
    </InitialChatStack.Navigator>
  );
};

export default InitialChat;
