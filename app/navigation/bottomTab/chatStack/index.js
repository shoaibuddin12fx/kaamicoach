import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import ChatScreen from '../../../screens/ChatStackScreens/Chat';
import SpecialistCategoryScreen from '../../../screens/ChatStackScreens/SpecialistCategory';
import SpecialistListScreen from '../../../screens/ChatStackScreens/SpecialistList';
import SpecialistDetailsScreen from '../../../screens/ChatStackScreens/SpecialistDetails';

const ChatStack = createStackNavigator();

const Chat = () => {
  return (
    <ChatStack.Navigator headerMode="none" initialRouteName="Chat">
      <ChatStack.Screen name="Chat" component={ChatScreen} />
      <ChatStack.Screen
        name="SpecialistCategory"
        component={SpecialistCategoryScreen}
      />
      <ChatStack.Screen
        name="SpecialistList"
        component={SpecialistListScreen}
      />
      <ChatStack.Screen
        name="SpecialistDetails"
        component={SpecialistDetailsScreen}
      />
    </ChatStack.Navigator>
  );
};

export default Chat;
