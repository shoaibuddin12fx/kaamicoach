import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import ProfileScreen from '../../../screens/ProfileStackScreens/Profile';
import AddGoalScreen from '../../../screens/ProfileStackScreens/AddGoal';
import EditGoalScreen from '../../../screens/ProfileStackScreens/EditGoal';
import SettingsScreen from '../../../screens/ProfileStackScreens/Settings';
import EditFamilyScreen from '../../../screens/ProfileStackScreens/EditFamily';

const ProfileStack = createStackNavigator();

const Profile = () => {
  return (
    <ProfileStack.Navigator headerMode="none" initialRouteName="Profile">
      <ProfileStack.Screen name="Profile" component={ProfileScreen} />
      <ProfileStack.Screen name="AddGoal" component={AddGoalScreen} />
      <ProfileStack.Screen name="EditGoal" component={EditGoalScreen} />
      <ProfileStack.Screen name="Settings" component={SettingsScreen} />
      <ProfileStack.Screen name="EditFamily" component={EditFamilyScreen} />
    </ProfileStack.Navigator>
  );
};

export default Profile;
