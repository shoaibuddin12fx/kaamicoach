import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Chat from './chatStack';
import Dashboard from './dashboardStack';
import Newsfeed from './newsfeedStack';
import Profile from './profileStack';
import HomeSvg from '../../assets/svg/components/bottomTab/HomeSvg';
import ChatSvg from '../../assets/svg/components/bottomTab/ChatSvg';
import NewsfeedSvg from '../../assets/svg/components/bottomTab/NewsfeedSvg';
import ProfileSvg from '../../assets/svg/components/bottomTab/ProfileSvg';
import colors from '../../constants/color';

const BottomTabNav = createBottomTabNavigator();

const BottomTab = () => {
  return (
    <BottomTabNav.Navigator
      initialRouteName="DashboardTab"
      tabBarOptions={{
        showLabel: false,
        tabStyle: {
          paddingTop: 10,
          backgroundColor: colors.white,
          shadowOpacity: 0.04,
          shadowOffset: {
            width: 5,
            height: -5,
          },
        },
      }}>
      <BottomTabNav.Screen
        name="DashboardTab"
        component={Dashboard}
        options={{
          tabBarIcon: ({focused}) => (
            <HomeSvg color={focused ? colors.seagreen : colors.lightGrey} />
          ),
        }}
      />
      <BottomTabNav.Screen
        name="ChatTab"
        component={Chat}
        options={{
          tabBarIcon: ({focused}) => (
            <ChatSvg color={focused ? colors.seagreen : colors.lightGrey} />
          ),
        }}
      />
      <BottomTabNav.Screen
        name="NewsfeedTab"
        component={Newsfeed}
        options={{
          tabBarIcon: ({focused}) => (
            <NewsfeedSvg color={focused ? colors.seagreen : colors.lightGrey} />
          ),
        }}
      />
      <BottomTabNav.Screen
        name="ProfileTab"
        component={Profile}
        options={{
          tabBarIcon: ({focused}) => (
            <ProfileSvg color={focused ? colors.seagreen : colors.lightGrey} />
          ),
        }}
      />
    </BottomTabNav.Navigator>
  );
};

export default BottomTab;
