import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import DashboardScreen from '../../../screens/DashboardStackScreens/Dasboard';
import ActivityScreen from '../../../screens/DashboardStackScreens/Activity';

const DashboardStack = createStackNavigator();

const Dashboard = () => {
  return (
    <DashboardStack.Navigator headerMode="none" initialRouteName="Dashboard">
      <DashboardStack.Screen name="Dashboard" component={DashboardScreen} />
      <DashboardStack.Screen name="Activity" component={ActivityScreen} />
    </DashboardStack.Navigator>
  );
};

export default Dashboard;
