import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import NewsfeedScreen from '../../../screens/NewsfeedStackScreens/Newsfeed';
import ChosenTopicsScreen from '../../../screens/NewsfeedStackScreens/ChosenTopics';
import SavedArticlesScreen from '../../../screens/NewsfeedStackScreens/SavedArticles';
import ArticleScreen from '../../../screens/NewsfeedStackScreens/Article';

const NewsfeedStack = createStackNavigator();

const Newsfeed = () => {
  return (
    <NewsfeedStack.Navigator headerMode="none" initialRouteName="Newsfeed">
      <NewsfeedStack.Screen name="Newsfeed" component={NewsfeedScreen} />
      <NewsfeedStack.Screen
        name="ChosenTopics"
        component={ChosenTopicsScreen}
      />
      <NewsfeedStack.Screen
        name="SavedArticles"
        component={SavedArticlesScreen}
      />
      <NewsfeedStack.Screen name="Article" component={ArticleScreen} />
    </NewsfeedStack.Navigator>
  );
};

export default Newsfeed;
