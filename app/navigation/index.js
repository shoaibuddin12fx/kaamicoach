import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import Intro from './intro';
import Auth from './auth';
import InitialChat from './initialChat';
import BottomTab from './bottomTab';
import {useSelector} from 'react-redux';

const Navigation = () => {
  const showIntro = useSelector((state) => state.auth.showIntro);
  const showAuth = useSelector((state) => state.auth.showAuth);
  const showInitialChat = useSelector((state) => state.auth.showInitialChat);

  return (
    <NavigationContainer>
      {showIntro ? (
        <Intro />
      ) : showAuth ? (
        <Auth />
      ) : showInitialChat ? (
        <InitialChat />
      ) : (
        <BottomTab />
      )}
    </NavigationContainer>
  );
};

export default Navigation;
